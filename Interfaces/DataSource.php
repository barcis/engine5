<?php

namespace Engine5\Interfaces;

/**
 * Description of DataSource
 *
 * @author barcis
 */
interface DataSource {

    function getQuery($params);

    function getFields();

    function getData($params);
}
