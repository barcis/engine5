<?php

namespace Engine5\Interfaces;

/**
 * Description of Controller
 *
 * @author barcis
 */
interface Controller extends Container {

    public function __preInit(\Engine5\Core\ControllerViewArgs &$controllerViewArgs);

    public function hasErrors();

    public function getErrors();

    public function addError($error);

    public function __init();
}
