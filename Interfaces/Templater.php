<?php

namespace Engine5\Interfaces;

/**
 * Description of Session
 *
 * @author barcis
 */
interface Templater {

    const RUN_MODE_DRY = 'Dry';
    const RUN_MODE_WET = 'Wet';

    function init(\Engine5\Core\Engine\App\Config\Templater $config);

    /**
     *
     * @return string
     */
    function fetch($file, $dir, $caller = null);

    function setContainer(\Engine5\Interfaces\Controller $container);

    function setDryRun($bool);
}
