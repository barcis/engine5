<?php

namespace Engine5\Interfaces;

/**
 * Description of Session
 *
 * @author barcis
 */
interface Session {

    static function free();

    static function getInstance();

    function get($field);

    function set($field, $value);

    function __destruct();
}
