<?php

namespace Engine5\Core;

/**
 * Description of Region
 *
 * @author barcis
 */
abstract class DataSource implements \Engine5\Interfaces\DataSource {

    /**
     * @return \SORM\Query
     */
    protected $params;

    public function __construct($params) {
        $this->params = $params;
    }

    abstract function getQuery($params);

    protected function afterGetData($params, &$data) {

    }

    public final function getData($params) {
        \Engine5\Core\Debug::beginTimeLogSession();
        $sql = $this->getQuery($params);
        if ($sql instanceof \SORM\Query) {
            $sql->fields($this->getFields());
        }
        $way = '?';
        $md5 = md5($sql->getSql());
        try {
            $data = \Engine5\Cache\Core::instance()->get('datasource', $md5);
            $way = 'from-cache';
        } catch (\Engine5\Cache\CacheExpireException $exc) {
            if ($sql instanceof \SORM\Query) {
                $data = $sql->select();
            } else {
                $data = $sql;
            }
            \Engine5\Cache\Core::instance()->set('datasource', $md5, $data);
            $way = 'from-database';
        }

        $time = 'DataSource|' . get_class($this) . '|' . $way . ': ' . \Engine5\Core\Debug::endTimeLogSession()->execution . "\n";
        file_put_contents(ST_VAR . '/regions.log', $time, FILE_APPEND);

        $this->afterGetData($params, $data);

        return $data;
    }

    public final function getCount($params) {
        $sql = $this->getQuery($params);
        if ($sql instanceof \SORM\Query) {
            $sql->fields($this->getFields());
        }
        $md5 = md5($sql->getSql()) . 'count';

        try {
            $data = \Engine5\Cache\Core::instance()->get('datasource', $md5);
        } catch (\Engine5\Cache\CacheExpireException $exc) {
            $data = $sql->count();

            \Engine5\Cache\Core::instance()->set('datasource', $md5, $data);
        }
        return $data;
    }

    public static function getPosibleParams() {
        return [];
    }

    static function translateParameters(array $params) {

        $rc = new \ReflectionClass(get_called_class());

        $posible = $rc->getMethod('getPosibleParams')->invoke(NULL);

        foreach ($posible as $k => $v) {
            $posible[$k] = (object) $v;
        }
        foreach ($params as $name => $value) {
            if (isset($posible[$name])) {
                $posible[$name]->value = $value;
            } elseif (in_array($name, ['limit', 'offset'])) {
                $posible[$name] = $value;
            }
        }


        return (object) $posible;
    }

    public function getFields() {
        return [];
    }

}
