<?php

namespace Engine5\Core\Templater\Smarty\Plugins\Blocks;

class Line {

    public static function register(\Smarty $smarty) {
        $smarty->registerPlugin('block', 'line', array(__CLASS__, 'render'));
    }

    public static function render($params, $text, \Smarty_Internal_Template &$template, &$repeat) {
        if (!isset($params['field']) || !($params['field'] instanceof \Engine5\Core\Form\Field)) {
            if (isset($params['name'])) {
                $fields = $template->getTemplateVars('fields');
                if (!isset($fields[$params['name']])) {
                    return '';
                }
                $params['field'] = $fields[$params['name']];
                unset($params['name']);
            } else {
                return '';
            }
        }

        $out = '';
        if (!$repeat) {
            $kparams = array_keys($params);
            $datasOut = '';
            $attrsOut = '';
            $datas = preg_grep('/^data_[a-zA-Z0-9\-\_]+$/', $kparams);
            $attrs = preg_grep('/^attr_[a-zA-Z0-9\-\_]+$/', $kparams);

            if (is_array($datas) && count($datas) > 0) {
                foreach ($datas as $key) {
                    $k = preg_replace('/^data_/', '', $key);
                    $datasOut .= " data-{$k}='{$params[$key]}'";
                    unset($params[$key]);
                }
            }
            if (is_array($attrs) && count($attrs) > 0) {
                foreach ($attrs as $key) {
                    $k = preg_replace('/^data_/', '', $key);
                    $attrsOut .= " {$k}='{$params[$key]}'";
                    unset($params[$key]);
                }
            }

            if (isset($params['template'])) {
                $tpl = $params['template'];
                unset($params['template']);
                $template->assign('__attrs', ($datasOut . $attrsOut));

                $out = $template->fetch(ST_APPDIR . '/smarty/line/' . $tpl . '.tpl');

                $template->clearAssign('__attrs');
                return $out;
            }

            $field = $params['field'];
            unset($params['field']);

            $out = '<div class="line"' . $datasOut . $attrsOut;

            if ($field instanceof E_field_textarea || $field instanceof E_field_wysiwyg || $field instanceof E_field_photo) {
                $out .= ' style="height:auto"';
            }
            $out .= '>' . $text . '<div class="ff"></div></div>';
            foreach ($params as $name => $param) {
                $template->clearAssign($name);
            }
        } else {
            unset($params['field']);
            foreach ($params as $name => $param) {
                $template->assign($name, $param);
            }
        }
        return $out;
    }

}
