<?php

namespace Engine5\Core\Templater\Smarty\Plugins\Blocks;

class IfContent {

    public static function register(\Smarty $smarty) {
        $smarty->registerPlugin('block', 'ifcontent', array(__CLASS__, 'render'));
    }

    public static function render($params, $text, \Smarty_Internal_Template &$template, &$repeat) {
        if (!$repeat) {
            if (!isset($params['name'])) {
                return 'brak paramteru name dla tagu "ifcontent"';
            }
            $name = $params['name'];
            $_this = $template->getTemplateVars('this');

            if (isset($_this->section->type->fields)) {
                $fields = new \Engine5\JSON\ObjectList($_this->section->type->fields);
                $field = $fields->find('name', $name);

                if (!$field) {
                    return '';
                }

                $lang = '';
                if (isset($field->langs) && $field->langs) {
                    $lang = \Engine5\Core\Engine::getCurrentLanguage();
                    $dafaultLang = \Engine5\Core\Engine::getCurrentApp()->getDefaultLanguageId();
                }

                switch ($field->type) {
                    case 'image':
                        if (!isset($_this->content->{$name}) || !isset($_this->content->{$name}->data)) {
                            return '';
                        }
                        break;

                    case 'a-href':
                        if (!isset($_this->content->{$name}) || !isset($_this->content->{$name}->href)) {
                            return '';
                        }
                        break;

                    case 'text':
                    case 'textarea':
                    case 'wysiwyg':

                        if (isset($field->langs) && $field->langs) {
                            if (!isset($_this->content->{$name}->{$lang}) || !isset($_this->content->{$name}->{$dafaultLang})) {
                                return '';
                            }
                        } elseif (!isset($_this->content->{$name})) {
                            return '';
                        }
                        break;
                }
            }

            return $text;
        }

        return '';
    }

}
