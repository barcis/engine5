<?php

namespace Engine5\Core\Templater\Smarty\Plugins\Blocks;

class Translate {

    public static function register(\Smarty $smarty) {
        $smarty->registerPlugin('block', '_', array(__CLASS__, 'render'));
    }

    public static function render($params, $text, \Smarty_Internal_Template &$template, &$repeat) {

        if ($repeat) {
            return '';
        }

        return \Engine5\Core\Translate::_(stripslashes($text));
    }

}
