<?php

namespace Engine5\Core\Templater\Smarty\Plugins\Blocks;

class Form {

    public static function register(\Smarty $smarty) {
        $smarty->registerPlugin('block', 'form', array(__CLASS__, 'render'));
    }

    public static function render($params, $text, \Smarty_Internal_Template &$template, &$repeat) {
        $t = $template->getTemplateVars('this');
        $errors = isset($params['errors']) ? (bool) $params['errors'] : true;


        if (!isset($params['name'])) {
            //throw new \SmartyException("form wymaga parametru 'name'!");
            //formularz z cms-a
            $name = '_cmsautoform_';
        } else {
            $name = $params['name'];
        }
        unset($params['name'], $params['errors']);

        $form = \Engine5\Core\Form\Manager::getInstance($t->getName())->findForm($name);
        if (!( $form instanceof \Engine5\Core\Form )) {
            throw new \SmartyException('Błąd! Nie zdefiniowano formularza "' . $name . '"');
        }
        $out = '';
        if (!$repeat) {
            if ($form->isVisible() && isset($params['template'])) {
                $tpl = $params['template'];
                unset($params['template']);
                $text = $template->fetch(ST_APPDIR . '/smarty/form/' . $tpl . '.tpl');
            }

            $uri = \Engine5\Core\Engine::getCurrentUrl();

            $out = '<form target="_self" action="' . $uri . '"';
            foreach (array_merge($form->getAttributes(), $params) as $attr => $val) {
                $out .= " {$attr}='{$val}'";
            }

            $out .= ">";

            if ($form->hasFile()) {
                $out .= '<input type="hidden" name="MAX_FILE_SIZE" value="' . $form->getMaxFileSize() . '" />';
            }

            if ($errors && $form->hasErrors()) {
                $out .= '<ul class=\'form-error\'>';
                $errors = $form->getErrors();
                foreach ($errors as $error) {
                    $out .= '<li>' . $error->text . '</li>';
                }
                $out .= '</ul>';
            }
            if ($form->hasInfos()) {
                $out .= '<ul class=\'form-info\'>';
                $infos = $form->getInfos();
                foreach ($infos as $info) {
                    $out .= '<li>' . $info->text . '</li>';
                }
                $out .= '</ul>';
            }

            if ($form->isVisible()) {
                $out .= $text;
            }
            $out .= "</form>";
            $template->clearAssign('fields', '__parentFORM');
        } else {
            $template->assign('tabs', $form->getTabs());
            $template->assign('fields', $form->getFields());
            $template->assignByRef('__parentFORM', $form);
        }

        return $out;
    }

}
