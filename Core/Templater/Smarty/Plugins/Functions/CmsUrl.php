<?php

namespace Engine5\Core\Templater\Smarty\Plugins\Functions;

class CmsUrl {

    public static function register(\Smarty $smarty) {
        $smarty->registerPlugin('function', 'cmsurl', array(__CLASS__, 'render'));
    }

    public static function render($params, \Smarty_Internal_Template &$template) {
        if (!isset($params['site'])) {
            throw new \SmartyException("cmsurl wymaga parametru 'site'!");
        }

        $urlParams = $params;
        unset($urlParams['site']);

        return \CMS\Tools\Link::url($params['site'], $urlParams);
    }

}
