<?php

namespace Engine5\Core\Templater\Smarty\Plugins\Functions;

use Engine5\Core\Plugin\Manager as PluginManager;

class Plugin {

    public static function register(\Smarty $smarty) {
        $smarty->registerPlugin('function', 'plugin', array(__CLASS__, 'plugin'));
    }

    public static function plugin($params, \Smarty_Internal_Template &$template) {
        if ((!isset($params['hook']) && !isset($params['hook']))) {
            throw new SmartyException("plugin wymaga parametru 'hook'!");
        }

        $html = '';
        $regions = PluginManager::getRegionsForHook($params['hook']);
        foreach ($regions as $region) {
            $html .= Region::render($region, $template);
        }
        return $html;
    }

}
