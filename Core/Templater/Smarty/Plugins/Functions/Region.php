<?php

namespace Engine5\Core\Templater\Smarty\Plugins\Functions;

class Region {

    public static function register(\Smarty $smarty) {
        $smarty->registerPlugin('function', 'region', array(__CLASS__, 'render'));
    }

    public static function render($params, \Smarty_Internal_Template &$template) {
        $runMode = $template->getTemplateVars('runMode');

        if (!isset($params['name'])) {
            throw new \SmartyException("region wymaga parametru 'name'!");
        }
        if (!isset($params['view'])) {
            $params['view'] = 'default';
        }
        if (!isset($params['_params'])) {
            $params['_params'] = [];
        } elseif (isset($params['_params']) && is_object($params['_params'])) {
            $params['_params'] = (array) $params['_params'];
        } elseif (isset($params['_params']) && !is_array($params['_params'])) {
            $params['_params'] = json_decode($params['_params'], true);
            if (!is_array($params['_params'])) {
                $params['_params'] = [];
            }
        }
        if (isset($_COOKIE['DEBUG_REGION']) && $_COOKIE['DEBUG_REGION'] === 'yes') {
            \Engine5\Core\Debug::beginTimeLogSession();
        }

        $args = $params;
        unset($args['name'], $args['view'], $args['_params']);

        $container = $template->getTemplateVars('container');
        /* @var $container \Engine5\Interfaces\Controller */
        $ph = $container->getPlaceHolder('__INLINE_PLACEHOLDER__');

        $am = array_merge($container->params, $args, $params['_params']);

        $out = $ph->renderRegion($runMode, $params['name'], $params['view'], $am);

        if (isset($_COOKIE['DEBUG_REGION']) && $_COOKIE['DEBUG_REGION'] === 'yes') {

            $time = 'render|' . $_SERVER['REQUEST_URI'] . '|' . $params['name'] . '::' . $params['view'] . '(' . implode(', ', array_keys($am)) . '): ' . \Engine5\Core\Debug::endTimeLogSession()->execution . var_export($runMode, true) . "\n";
            file_put_contents(ST_VAR . '/regions.log', $time, FILE_APPEND);
        }
        return $out;
    }

}
