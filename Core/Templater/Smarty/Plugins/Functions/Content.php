<?php

namespace Engine5\Core\Templater\Smarty\Plugins\Functions;

class Content {

    public static function register(\Smarty $smarty) {
        $smarty->registerPlugin('function', 'content', array(__CLASS__, 'render'));
    }

    public static function render($params, \Smarty_Internal_Template &$template) {
        if (!isset($params['name'])) {
            return 'brak paramteru name dla tagu "content"';
        }


        $name = $params['name'];
        $_this = $template->getTemplateVars('this');
        if (isset($_this->section->type->fields)) {
            $fields = new \Engine5\JSON\ObjectList($_this->section->type->fields);
            $field = $fields->find('name', $name);

            if (!$field) {
                return '';
            }
            $lang = '';
            if (isset($field->langs) && $field->langs) {
                $lang = \Engine5\Core\Engine::getCurrentLanguage();
                $dafaultLang = \Engine5\Core\Engine::getCurrentApp()->getDefaultLanguageId();
            }

            switch ($field->type) {
                case 'date':
                    if (!isset($_this->content->{$name}) || !isset($_this->content->{$name}->data)) {
                        return '';
                    }

                    return 'ssss';
                    //return $_this->content->{$name}->data;
                    break;

                case 'image':
                    if (!isset($_this->content->{$name}) || !isset($_this->content->{$name}->data)) {
                        return '';
                    }
                    return $_this->content->{$name}->data;
                    break;

                case 'a-href':
                    if (!isset($_this->content->{$name}) || !isset($_this->content->{$name}->href)) {
                        return '';
                    }
                    return $_this->content->{$name}->href;
                    break;
                case 'text':
                case 'textarea':
                case 'wysiwyg':

                    if (isset($field->langs) && $field->langs) {
                        if (isset($_this->content->{$name}->{$lang})) {
                            return $_this->content->{$name}->{$lang};
                        } elseif (isset($_this->content->{$name}->{$dafaultLang})) {
                            return $_this->content->{$name}->{$dafaultLang};
                        }
                    }

                    if (isset($_this->content->{$name})) {
                        if (!is_object($_this->content->{$name})) {
                            return $_this->content->{$name};
                        }
                    } elseif (isset($field->default)) {
                        return $field->default;
                    }
                    break;

                default:
                    if (isset($_this->content->{$name})) {
                        return $_this->content->{$name};
                    } elseif (isset($field->default)) {
                        return $field->default;
                    }
                    break;
            }
        } else {
            return '';
        }
    }

}
