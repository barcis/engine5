<?php

namespace Engine5\Core\Templater\Smarty\Plugins\Functions;

class Label {

    public static function register(\Smarty $smarty) {
        $smarty->registerPlugin('function', 'label', array(__CLASS__, 'render'));
    }

    public static function render($params, \Smarty_Internal_Template &$template) {
        if ((!isset($params['item']) && !isset($params['name']))) {
            throw new SmartyException("label wymaga parametru 'item' lub nazwy pola 'name'!");
        }
        if (!isset($params['append'])) {
            $params['append'] = ':';
        }
        if (!isset($params['required'])) {
            $params['required'] = '<span class=\'req\'>*</span>';
        }


        if (!isset($params['item'])) {
            $fields = $template->getTemplateVars('fields');
            if (!isset($fields[$params['name']]) && $params['ifExist'] !== true) {
                throw new SmartyException("nieprawidłowa nazwa 'name' pola formularza do wylabelowania, pole  '{$params['name']}' nie istnieje");
            } elseif (!isset($fields[$params['name']]) && $params['ifExist'] === true) {
                return '';
            }
            $params['item'] = $fields[$params['name']];
            unset($params['name']);
        }

        if (!($params['item'] instanceof \Engine5\Core\Form\Field)) {
            throw new \SmartyException("label wymaga by parametr 'item' dziedziczył z E_field!");
        }

        /* @var E_form $form */
        $form = $template->getTemplateVars('__parentFORM');
        $item = $params['item']; /* @var E_field $item */
        unset($params['item']);

        $out = $item->getLabel() . ($item->isRequired() ? '&#160;' . $params['required'] . '&#160;' : '' ) . $params['append'];

        if ($form->getLabelTags() || (isset($params['tags']) && $params['tags'])) {
            $out = "<label for=\"{$form->getName()}-{$item->getName()}\">{$out}</label>";
        }
        return $out;
    }

}
