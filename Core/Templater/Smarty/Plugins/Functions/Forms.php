<?php

namespace Engine5\Core\Templater\Smarty\Plugins\Functions;

class Forms {

    public static function register(\Smarty $smarty) {
        $smarty->registerPlugin('function', 'render', array(__CLASS__, 'render'));
        $smarty->registerPlugin('function', 'errormsg', array(__CLASS__, 'errormsg'));
    }

    public static function errormsg($params, \Smarty_Internal_Template &$template) {
        if ((!isset($params['item']) && !isset($params['name']))) {
            throw new \SmartyException("label wymaga parametru 'item' lub nazwy pola 'name'!");
        }

        if (!isset($params['item'])) {
            $fields = $template->getTemplateVars('fields');
            if (!isset($fields[$params['name']]) && $params['ifExist'] !== true) {
                throw new \SmartyException("nieprawidłowa nazwa 'name' pola formularza do wylabelowania, pole  '{$params['name']}' nie istnieje");
            } elseif (!isset($fields[$params['name']]) && $params['ifExist'] === true) {
                return '';
            }
            $params['item'] = $fields[$params['name']];
            unset($params['name']);
        }

        if (!($params['item'] instanceof \Engine5\Core\Form\Field)) {
            throw new \SmartyException("label wymaga by parametr 'item' dziedziczył z \Engine5\Core\Form\Field!");
        }


        $form = $template->getTemplateVars('__parentFORM');
        $_this = $template->getTemplateVars('this');

        /* @var \Engine5\Core\Form $form */

        $item = $params['item']; /* @var \Engine5\Core\Form\Field $item */
        unset($params['item']);

        $message = array();
        foreach ($item->getErrors() as $msg) {
            $message[] = $msg->text;
        }
        if (!$message) {
            return '';
        }
        return '<span class="f-error">' . implode(' ', $message) . "</span>";
    }

    public static function render($params, \Smarty_Internal_Template &$template) {
        if ((!isset($params['item']) && !isset($params['name']))) {
            throw new \SmartyException("render wymaga parametru 'item' lub nazwy pola ('name')!");
        }

        $ifExist = isset($params['ifExist']) ? $params['ifExist'] : null;

        unset($params['ifExist']);

        if (!isset($params['item'])) {
            $fields = $template->getTemplateVars('fields');
            if (!isset($fields[$params['name']]) && $ifExist !== true) {
                throw new \SmartyException("nieprawidłowa nazwa (name) pola formularza do wyrenderowania, pole  '{$params['name']}' nie istnieje");
            } elseif (!isset($fields[$params['name']]) && $ifExist === true) {
                return '';
            }
            $params['item'] = $fields[$params['name']];
            unset($params['name']);
        }

        if (!($params['item'] instanceof \Engine5\Core\Form\Field)) {
            throw new \SmartyException("render wymaga by parametr 'item' dziedziczył z \Engine5\Core\Form\Field!");
        }

        $form = $template->getTemplateVars('__parentFORM');
        $_this = $template->getTemplateVars('this');

        /* @var $form \Engine5\Core\Form  */

        $item = $params['item'];
        /* @var $item \Engine5\Core\Form\Field  */
        unset($params['item']);

        if ($item instanceof EI_extendendRendering) {
            $out = $item->extendendRendering($_this->getName(), $form->getName());
        } else {
            $out = '<' . $item->getTag();
            if ($item->getType() == 'E_field_checkbox' && isset($params['checked']) && $params['checked'] != 'checked') {
                unset($params['checked']);
            }
            $attributes = array_merge($item->getAttributes($_this->getName(), $form->getName()), $params);

            if (!isset($attributes['class'])) {
                $attributes['class'] = '';
            }

            if ($item->hasErrors()) {
                $attributes['class'] .= ' f-errors';
            }

            if ($item->isRequired()) {
                $attributes['class'] .= ' f-required';
                $attributes['required'] = 'required';
            }

            if ($item->isDisabled()) {
                $attributes['disabled'] = 'disabled';
            }

            $attributes['placeholder'] = $item->getPlaceholder();

            foreach ($attributes as $attr => $val) {
                if (!is_array($val)) {
                    $attr = trim($attr);
                    $val = trim($val);
                    $out .= " {$attr}='{$val}'";
                }
            }

            $out .= " data-label='{$item->getLabel()}'";

            if (count($item->getE5JsAttributes()) > 0 || count($item->getValidators()) > 0) {
                $out .= " data-e5='engine5'";
                foreach ($item->getE5JsAttributes() as $attr => $val) {
                    $attr = trim($attr);
                    $val = trim($val);
                    $out .= " data-e5-{$attr}='{$val}'";
                }

                if ($item->getValidators()) {
                    $out .= " data-e5-validators='" . implode(',', $item->getValidators()) . "'";
                }
            }

            $out .= $item->isShort() ? '/>' : '>' . $item->renderValue() . '</' . $item->getTag() . '>';
        }
        return $out;
    }

}
