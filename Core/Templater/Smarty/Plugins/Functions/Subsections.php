<?php

namespace Engine5\Core\Templater\Smarty\Plugins\Functions;

class Subsections {

    public static function register(\Smarty $smarty) {
        $smarty->registerPlugin('function', 'subsections', array(__CLASS__, 'render'));
    }

    private static function renderRegion($cmsSection, $defaultController, \Smarty_Internal_Template &$template) {
        if (isset($_COOKIE['DEBUG_REGION']) && $_COOKIE['DEBUG_REGION'] === 'yes') {
            \Engine5\Core\Debug::beginTimeLogSession();
        }

        if (isset($cmsSection->type->controller)) {

            if (strpos($cmsSection->type->controller, '::')) {
                list($sectionController, $sectionView) = explode('::', $cmsSection->type->controller);
            } else {
                $sectionController = $cmsSection->type->controller;
                $methodName = lcfirst(str_replace(' ', '', ucwords(str_replace('-', ' ', $cmsSection->type->name))));
                $rc = new \ReflectionClass($sectionController);
                $sectionView = $rc->hasMethod($methodName . 'Init') ? $methodName : 'default';
            }
        } elseif (isset($cmsSection->type->name)) {
            $sectionView = $cmsSection->type->name;
            $sectionController = $defaultController;
        } else {
            return '<pre>Sekcja "' . $cmsSection->title . '" nie ma ustawionego typu :(</pre>';
        }

        $params = [
            'name' => $sectionController,
            'view' => $sectionView,
            'section' => $cmsSection,
        ];

        $html = Region::render($params, $template);
        if (isset($_COOKIE['DEBUG_REGION']) && $_COOKIE['DEBUG_REGION'] === 'yes') {
            $time = 'subsections|' . $cmsSection->type->name . ': ' . \Engine5\Core\Debug::endTimeLogSession()->execution . "\n";
            file_put_contents(ST_VAR . '/regions.log', $time, FILE_APPEND);
        }
        return $html;
    }

    public static function render($params, \Smarty_Internal_Template &$template) {
        $_this = $template->getTemplateVars('this');
        if (!isset($_this->sections) || !is_array($_this->sections)) {
            return '';
        }

        $rc = new \ReflectionClass(\Engine5\Core\Engine::getCurrentAppClassName());
        $defaultController = $rc->getNamespaceName() . '\Region\CmsSections';

        $content = '<!-- [subsections] -->';

        if (isset($_this->section->type->children->display) && $_this->section->type->children->display === 'list') {
            foreach ($_this->sections as $cmsSection) {
                $content .= self::renderRegion($cmsSection, $defaultController, $template);
            }
        } else {
            foreach ($_this->section->getRows() as $idx => $row) {
                $offset = 0;
                $content .= '
    <div class="row row-idx-' . $idx . '">';
                for ($i = 0; $i < 12; $i++) {
                    if (isset($row[$i])) {
                        $cmsSection = $row[$i];
                        $offsetPart = " col-idx-{$i}";
                        if ($offset > 0) {
                            $offsetPart .= " col-sm-offset-{$offset}";
                        }

                        if (isset($cmsSection->sizes) && isset($cmsSection->sizes->lg)) {
                            $content .= '
                <div class="col-sm-' . $cmsSection->sizes->lg . $offsetPart . '">';
                        } elseif (isset($cmsSection->sizeX)) {
                            $content .= '
                <div class="col-sm-' . $cmsSection->sizeX . $offsetPart . '">';
                        }

                        $content .= self::renderRegion($cmsSection, $defaultController, $template);

                        $content .= '
                </div>';
                        if (isset($cmsSection->sizes) && isset($cmsSection->sizes->lg)) {
                            $i += ($cmsSection->sizes->lg - 1);
                        } else {
                            $i += ($cmsSection->sizeX - 1);
                        }
                        $offset = 0;
                    } else {
                        $offset++;
                    }
                }
                $content .= '
    </div>';
            }
        }
        return $content . '<!-- [/subsections] -->';
    }

}
