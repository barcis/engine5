<?php

namespace Engine5\Core\Templater\Smarty\Plugins\Functions;

class Placeholder {

    public static function register(\Smarty $smarty) {
        $smarty->registerPlugin('function', 'placeholder', array(__CLASS__, 'render'));
    }

    public static function render($params, \Smarty_Internal_Template &$template) {
        if (!isset($params['name'])) {
            throw new \SmartyException("placeholder wymaga parametru 'name'!");
        }

        $container = $template->getTemplateVars('container');
        $runMode = $template->getTemplateVars('runMode');
        /* @var $container EI_container */
        if (!isset($container)) {
            throw new \SmartyException("placeholder użyto w zbyt zacnym celu :)!");
        }

        if (!($container instanceof \Engine5\Interfaces\Container)) {
            throw new \SmartyException("placeholder: container jest niepoprawny!");
        }

        $ph = $container->getPlaceHolder($params['name']);

        if (!isset($ph)) {
            return '';
        }

        if ($ph->used()) {
            throw new \SmartyException("nazwa placeholdera musi być unikalna!");
        }

        $out = $ph->render($runMode);

        return $out;
    }

}
