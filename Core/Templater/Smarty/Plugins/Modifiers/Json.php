<?php

namespace Engine5\Core\Templater\Smarty\Plugins\Modifiers;

class Json implements \Engine5\Interfaces\Templater\Smarty\Plugin {

    public static function register(\Smarty $smarty) {
        $smarty->registerPlugin('modifier', 'json', array(__CLASS__, 'json'));
    }

    public static function json($object) {

        return json_encode($object, 128);
    }

}
