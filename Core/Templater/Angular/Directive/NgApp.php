<?php

namespace Engine5\Core\Templater\Angular\Directive;

class NgApp extends \Engine5\Core\Templater\Angular\Directive {

    public function apply() {
        $this->element->removeAttribute('ng-app');
    }

}
