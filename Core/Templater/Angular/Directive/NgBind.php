<?php

namespace Engine5\Core\Templater\Angular\Directive;

class NgBind extends \Engine5\Core\Templater\Angular\Directive {

    protected function parseValue($matches) {
        $value = trim($matches['value']);

        //$filters = explode('|', trim($matches['filters'], ' |'));
        //print_R($filters);
//        $scopeValue = $this->scope->getByPath($value);
//
//        if ($scopeValue === null) {
//            return '{{#' . $matches['value'] . '#}}';
//        }
//        return $scopeValue;

        try {
            $ev = new \Engine5\Tools\Evaluator($value);
            $result = $ev->evaluate($this->scope);
        } catch (\Exception $e) {
            return '{{#' . $matches['value'] . '#}}';
        }

        return $result;
    }

    public function apply() {
        if (strlen(trim($this->element->wholeText)) === 0) {
            return;
        }

        $this->element->nodeValue = \preg_replace_callback('|{{\s*(?<value>[a-z0-9\.\-\$\ \+]+)\s*(\|(?<filters>[a-z0-9\.\-\:\|]*))?\s*}}|im', [$this, 'parseValue'], $this->element->wholeText);
    }

}
