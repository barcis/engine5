<?php

namespace Engine5\Core\Templater\Angular\Directive;

use Engine5\Core\Resource\Manager as ResourceManager;

class NgScripts extends \Engine5\Core\Templater\Angular\Directive {

    public function apply() {
        $rand = rand(1000, 9999);
        $res = ResourceManager::getInstance()->get('app');

        $parent = $this->element->parentNode;
        foreach (array_keys($res) as $value) {
            $node = $this->element->ownerDocument->createElement('script');

            $attrType = $this->element->ownerDocument->createAttribute('type');
            $attrType->value = 'text/javascript';

            $attrSrc = $this->element->ownerDocument->createAttribute('src');
            $attrSrc->value = "/app/{$value}.js?rand={$rand}";
            $node->appendChild($attrType);
            $node->appendChild($attrSrc);

            $parent->insertBefore($node, $this->element);
        }
        $parent->removeChild($this->element);
    }

}
