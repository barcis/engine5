<?php

namespace Engine5\Core\Templater\Angular\Directive;

use Analog\Analog;

class NgInclude extends \Engine5\Core\Templater\Angular\Directive {

    public function apply() {
        $attrs = $this->attributes();
        $attrValue = $attrs[$this->name];
        $this->element->removeAttribute($this->name);

        if (preg_match("/\'[\w\.\-\/]+\'/", $attrValue)) {
            $file = trim($attrValue, '\'');
        } else {
            $file = $this->scope[$attrValue];
        }
        $options = $this->module->options();
        $filename = $options['dir'] . $file;

        $fragment = new \DOMDocument();
        $fragment->loadHTMLFile($filename);

        $body = $fragment->getElementsByTagName('body')->item(0);
        $nodes = [];
        for ($i = 0; $i < $body->childNodes->length; $i++) {
            $node = $this->element->ownerDocument->importNode($body->childNodes->item($i), true);
            $this->element->appendChild($node);
            $nodes[] = $this->module->parser->parse($node, $this->scope);
        }

        $this->node->nodes($nodes);
    }

}
