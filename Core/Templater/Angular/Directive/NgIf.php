<?php

namespace Engine5\Core\Templater\Angular\Directive;

class NgIf extends \Engine5\Core\Templater\Angular\Directive {

    public function apply() {
        $if = $this->element->getAttribute($this->name);
        $v = $this->scope->getByPath($if);

        if (!$v) {
            $parent = $this->element->parentNode;
            $parent->removeChild($this->element);
            return;
        }
        $this->element->removeAttribute($this->name);
    }

}
