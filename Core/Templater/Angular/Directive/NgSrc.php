<?php

namespace Engine5\Core\Templater\Angular\Directive;

class NgSrc extends \Engine5\Core\Templater\Angular\Directive {

    public function __construct($element, $scope, $module, $attrName) {
        parent::__construct($element, $scope, $module, $attrName);
        $href = $this->element->attributes->getNamedItem($attrName);
        if (!($href instanceof \DOMAttr)) {
            return;
        }
        $attr = $this->element->ownerDocument->createAttribute('src');
        $attr->value = $href->value;
        $this->element->appendChild($attr);
        $this->element->removeAttribute($attrName);
    }

    public function apply() {

    }

}
