<?php

namespace Engine5\Core\Templater\Angular;

abstract class Directive {

    abstract function apply();

    /**
     *
     * @var Scope
     */
    protected $scope = null;

    /**
     *
     * @var \Engine5\Core\Templater\Angular\Module
     */
    protected $module = null;

    /**
     *
     * @var \Engine5\Core\Templater\Angular\DOMNode
     */
    protected $node = null;

    /**
     *
     * @var \DOMElement
     */
    protected $element = null;
    protected $name = null;

    public function __construct($element, $scope, $module, $attrName) {
        $this->scope = $scope;
        $this->element = $element;
        $this->module = $module;
        $this->name = $attrName;
    }

    public function link($scope, \DOMElement $element, $attributes = []) {

    }

    public function node($node) {
        $this->node = $node;
    }

    public function attributes() {
        $attributes = [];
        for ($j = 0; $j < $this->element->attributes->length; $j++) {
            $attribute = $this->element->attributes->item($j);
            $attributes[$attribute->name] = $attribute->value;
        }
        return $attributes;
    }

}
