<?php

namespace Engine5\Core\Templater\Angular;

class DOMNode {

    /**
     *
     * @var \DOMNode
     */
    protected $element = null;

    /**
     *
     * @var Directive[]
     */
    protected $directives = [];

    /**
     *
     * @var DOMNode[]
     */
    protected $childNodes = [];

    public function __construct($element, $directives) {
        $this->element = $element;
        $this->directives = $directives;

        foreach ($directives as $directive) {
            $directive->node($this);
        }
    }

    public function apply() {
        foreach ($this->directives as $directive) {
            /* @var $directive Directive */
            $directive->apply();
        }
        foreach ($this->childNodes as $node) {
            /* @var $node DOMNode */
            $node->apply();
        }
    }

    public function nodes($nodes) {
        $this->childNodes = $nodes;
    }

}
