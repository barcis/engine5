<?php

namespace Engine5\Core\Templater\Angular;

/**
 * inspired by Bazalt/Angular
 */
class Angular {

    /**
     *
     * @var \Engine5\Core\Templater\Angular
     */
    protected $templater;
    protected $modules = [];

    public function __construct(\Engine5\Core\Templater\Angular $templater) {
        $this->templater = $templater;
    }

    /**
     *
     * @param string $name
     * @param array $options
     * @return Module
     */
    public function module($name, $options = null) {
        if (!isset($this->modules[$name])) {
            $this->modules[$name] = new Module($name, $options, $this->templater);
        }

        return $this->modules[$name];
    }

}
