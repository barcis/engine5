<?php

namespace Engine5\Core\Templater\Angular;

class Scope implements \ArrayAccess {

    protected $variables = null;
    protected $parent = null;

    public function __construct($variables) {
        $this->variables = $variables;
    }

    public function getByPath($value) {
        if (isset($this->variables[$value])) {
            return $this->variables[$value];
        }

        $scope = $this->variables;
        $pathEntries = explode('.', $value);
        foreach ($pathEntries as $path) {
            if ((is_array($scope) || $scope instanceof \ArrayAccess)) {
                $scope = isset($scope[$path]) ? $scope[$path] : null;
            } elseif (is_object($scope)) {
                $scope = isset($scope->{$path}) ? $scope->{$path} : null;
            }
        }
        return $scope;
    }

    public function newScope() {
        $scope = new Scope();
        $scope->parent = $this;

        return $scope;
    }

    public function offsetExists($offset) {
        return isset($this->variables[$offset]);
    }

    public function offsetGet($offset) {
        return $this->variables[$offset];
    }

    public function offsetSet($offset, $value) {
        $this->variables[$offset] = $value;
    }

    public function offsetUnset($offset) {
        unset($this->variables[$offset]);
    }

}
