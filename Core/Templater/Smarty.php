<?php

namespace Engine5\Core\Templater;

use Engine5\Core\Engine;

//use Engine5\Core\Auth;

/**
 * Description of smarty
 *
 * @author barcis
 */
class Smarty extends \Engine5\Core\Templater {

    /**
     * @var \Smarty
     */
    private $smarty;
    private $templateDirs = array();

    public function init(Engine\App\Config\Templater $config) {
        $this->smarty = new \Smarty();

        $this->smarty->setCompileDir(ST_ROOTDIR . '/var/compiled/');

        $this->smarty->force_compile = $config->force_compile;
//        $this->smarty->caching = \Smarty::CACHING_LIFETIME_SAVED;
        $this->smarty->error_reporting = $config->error_reporting;
        $this->smarty->debugging = $config->debugging;
        $this->smarty->autoload_filters = $config->autoload_filters;
        $this->smarty->auto_literal = false;
        $this->smarty->left_delimiter = '[{';
        $this->smarty->right_delimiter = '}]';

        if (is_array($config->plugins)) {
            foreach ($config->plugins as $plugin) {
                $func = [$plugin, 'register'];
                call_user_func($func, $this->smarty);
            }
        }

        $_app = new \stdClass();
        $_app->base_address = Engine::getCurrentAppProto() . '://' . Engine::getCurrentAppDomain() . Engine::getCurrentAppPort() . Engine::getCurrentAppBaseUrl();
        $_app->base_address_short = trim(Engine::getCurrentAppProto() . '://' . Engine::getCurrentAppDomain() . Engine::getCurrentAppPort() . Engine::getCurrentAppBaseUrl(), '/');
        $this->smarty->assign('_app', $_app);
    }

    public function setContainer(\Engine5\Interfaces\Controller $container) {
        $this->smarty->assignByRef('container', $container);
    }

    public function setDryRun($bool) {
        $this->smarty->assign('runMode', $bool === true ? self::RUN_MODE_DRY : self::RUN_MODE_WET);
    }

    public function fetch($file, $dir, $caller = null) {
        array_unshift($this->templateDirs, $dir);
        try {
            if (isset($caller)) {
                $this->smarty->assign('this', $caller);
            }
            $callerName = get_class($caller);
            $rc = new \ReflectionClass($callerName);
            if ($rc->isSubclassOf('\Engine5\Core\Region')) {
                $d = dirname($rc->getFileName()) . '/views/' . $rc->getShortName() . '/';
            } else {
                $d = $dir;
            }

            $testedViews = [];
            $testedViews[] = $d . $file . '.tpl';

            while (!file_exists($d . $file . '.tpl') && $rc->getParentClass()) {
                $rc = $rc->getParentClass();
                $d = dirname($rc->getFileName()) . '/views/' . $rc->getShortName() . '/';
                $testedViews[] = $d . $file . '.tpl';
            }

            if (file_exists($d . $file . '.tpl')) {
                $this->smarty->setTemplateDir($d);
                $return = $this->smarty->fetch($file . '.tpl');
            } else {
                throw new \Exception("View file ( {$d}{$file}.tpl) is not exists!<br/>" . var_export($testedViews, true));
            }
        } catch (\Exception $e) {
            $return = $e->getMessage();
            $return .= ' (' . $e->getFile() . ':' . $e->getLine() . ') ';
        }
        array_shift($this->templateDirs);
        if (count($this->templateDirs) > 0) {
            $dir = reset($this->templateDirs);
            $this->smarty->setTemplateDir($dir);
        }

        return $return;
    }

}
