<?php

namespace Engine5\Core\Templater;

use Engine5\Core\Engine;

//use Engine5\Core\Auth;

/**
 * Description of smarty
 *
 * @author barcis
 */
class Angular extends \Engine5\Core\Templater {

    /**
     *
     * @var Angular\Module
     */
    private $module;
    private $dryRun;

    /**
     *
     * @var Engine\App\Config\Templater
     */
    private $config;

    public function init(Engine\App\Config\Templater $config) {
        $this->config = $config;
        $this->module = (new Angular\Angular($this))->module($config->ngApp);

        foreach ($config->ngDirectives as $directive => $options) {
            $this->module->directive($directive, $options);
        }

        foreach ($config->ngFilters as $filter => $class) {
            $this->module->filter($filter, $class);
        }
    }

    public function config() {
        return $this->config;
    }

    public function fetch($file, $dir, $caller = null) {
        if ($this->dryRun) {
            return;
        }
        $this->module->options([
            'dir' => ST_FRONTENDDIR,
            'file' => $file . '.html'
        ]);

        $scope = $caller->scope;

        foreach ($this->container->getPlaceHolder('default')->regions() as $region) {
            if (isset($this->container->getPlaceHolder('default')[$region]->scope)) {
                $s = $this->container->getPlaceHolder('default')[$region]->scope;
                if (is_array($s)) {
                    $scope = array_merge($scope, $s);
                }
            }
        }
        $this->module->scope($scope);

        return $this->module->html();
    }

    public function setContainer(\Engine5\Interfaces\Controller $container) {
        $this->container = $container;
    }

    public function setDryRun($bool) {
        $this->dryRun = $bool;
    }

}
