<?php

namespace Engine5\Core;

use Engine5\Interfaces\Templater AS ITemplater;
use Engine5\Core\Engine\Entrypoint;

/**
 * Description of Region
 *
 * @author barcis
 */
abstract class Region implements \Engine5\Interfaces\Container {

    public $title = '';

    /**
     * lista przekazanych parametrów do skryptu
     * @var array
     */
    public $params;
    public $messages = array();

    /**
     * @var Placeholders
     */
    private $placeholders;

    /**
     * @var string
     */
    private $currentView;

    /**
     *
     * @var integer
     */
    private $currentError = 0;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $return;
    public $random;

    /**
     * @var Fixed
     */
    private $_fix;
    private $_data;
    private $scope;

    /**
     * @var ITemplater
     */
    protected $templater;

    /**
     * @var ControllerViewArgs
     */
    protected $controllerViewArgs;

    protected function init() {

    }

    public function getScope() {
        return $this->scope;
    }

    public function __get($name) {
        if ($name == 'fix') {
            return $this->_fix;
        }

        if (isset($this->_data[$name])) {
            return $this->_data[$name];
        }
        return null;
    }

    public function __set($name, $value) {
        if ($name == 'fix') {
            throw new \Exception('nie wolno nadpisywać fix!');
        }

        $this->_data[$name] = $value;
    }

    final public function __construct($name, $view, ITemplater $templater = null, $params = null, ControllerViewArgs $controllerViewArgs = null) {
        $this->random = rand(1, 10000);
        $this->return = "";
        $this->params = $params;

        if (!is_array($this->params)) {
            $this->params = array();
        }

        $this->params['_'] = Engine::getCurrentAppDomainParams();
        $this->name = $name;
        $this->setView($view);
        $this->templater = &$templater;

        $this->controllerViewArgs = $controllerViewArgs;

        $this->_fix = Fixed::getInstance($name);
        $this->init();
        $funcViewName = lcfirst(str_replace(' ', '', ucwords(str_replace('-', ' ', $view)))) . 'Init';



        if (Engine::getEntryPoint() == Entrypoint::WWW) {
            if (method_exists($this, $funcViewName)) {
                if (Engine::getRequestMethod() === 'get') {
                    $p = Request::getParams();

                    if (isset($p['get']) && is_array($p['get']) && count($p['get']) > 0) {
                        $this->params['get'] = $p['get'];
                    }
                }
                call_user_func(array($this, $funcViewName));

                if ($this->currentError > 0) {
                    $this
                            ->controllerViewArgs
                            ->getControllerInstance($templater)
                            ->addError($this->currentError);
                }
            }
        } elseif (Engine::getEntryPoint() == Entrypoint::REST) {
            if (method_exists($this, $view)) {
                $requestMethod = Engine::getRequestMethod();
                $params = array();
                foreach ($controllerViewArgs->getRestArgsNames() as $param) {
                    if (isset($this->params[$param])) {
                        $params[] = $this->params[$param];
                    } elseif (isset($this->params[$requestMethod][$param])) {
                        $params[] = $this->params[$requestMethod][$param];
                    } else {
                        $params[] = null;
                    }
                }
                $this->return = call_user_func_array(array($this, $view), $params);
            } else {
                throw new \Exception("brak wywołanej metody " . $view . " w " . $name);
            }
        } elseif (Engine::getEntryPoint() == Entrypoint::AJAX) {
            if (method_exists($this, $view . "Ajax")) {
                $this->return = call_user_func(array($this, $view . "Ajax"));
            } else {
                throw new \Exception("brak wywołanej metody " . $view . "Ajax w " . $name);
            }
        }

        if (Request::isActiveRegions($name) !== false) {
            foreach (Request::isActiveRegions($name) as $action) {
                $fromManager = Form\Manager::getInstance($name)->findForm($action['form']);
                if (!($fromManager instanceof \Engine5\Core\Form)) {
                    break;
                }

                $action = $fromManager->getAction($action['field']);
                $callable = $action->getAction();
                //TODO: przenieść obsługę call_user_func do klasy \Engine5\Core\Form\Action
                if (is_callable($callable, false) && !$action->isDone()) {
                    $fromManager->postInit();
                    call_user_func($callable, $action);
                    $fromManager->postInit();
                    $action->setDone();
                }
            }
        }
    }

    final public function getName() {
        return $this->name;
    }

    /**
     * @param type $name
     * @param type $method
     * @return Form
     */
    final protected function form($name, $method = Form::POST) {
        return Form\Manager::getInstance($this->name)->form($name, $method);
    }

    final private function resource($type, $name, $priority) {
        Resource\Manager::getInstance($this->name)->add($type, $name, $priority);
    }

    final protected function js_code($code) {
        Resource\Manager::getInstance($this->name)->add_js_code($code);
    }

    final protected function js($name, $priority = 5) {
        return $this->resource(Resource\Manager::RESOURCE_JS, $name, $priority);
    }

    final protected function css($name, $priority = 5) {
        return $this->resource(Resource\Manager::RESOURCE_CSS, $name, $priority);
    }

    final protected function getRouter() {
        return \Engine5\Factory\Router::newInstance();
    }

    /**
     * @param string $view
     */
    final protected function setView($view) {
        $this->currentView = $view;
    }

    /**
     * @param int $error
     */
    final protected function setError($error) {
        $this->currentError = $error;
    }

    final protected function getView() {
        return $this->currentView;
    }

    /**
     * @return string
     */
    final public function render($runMode) {
        Context::setContext('region', $this->name);
        if (Engine::getEntryPoint() == Entrypoint::AJAX) {
            return $this->return;
        } elseif (Engine::getEntryPoint() == Entrypoint::REST) {
            if (!($this->return instanceof Rest\Result)) {
                throw new \Exception('Zwrócona wartość nie jest typem <b>Rest\Result</b>. <br/>Możliwe przyczyny: <ul><li>funkcja nie istnieje,</li><li>błędnie podana wartość w plik rest.config.yml</li><li>zwracana jest błędna wartość.</li></ul>');
                return;
            }
            $this->return->sendCode();
            return $this->return->getJSON();
        }

//        $out = '';
//        if ($runMode == ITemplater::RUN_MODE_DRY && method_exists($this, $this->currentView . 'View')) {
//            call_user_func(array($this, $this->currentView . 'View'));
//        } elseif ($runMode == ITemplater::RUN_MODE_WET) {
        $name = array_reverse(explode('\\', $this->name))[0];

        $dir = ST_APPDIR . '/Region/views/' . $name . '/';
        $out = $this->templater->fetch($name . '.' . $this->currentView, $dir, $this);

//        }
        Context::unsetContext();
        return $out;
    }

    /**
     * @param string $name
     * @return Placeholder
     */
    final public function getPlaceHolder($name) {
        if (isset($this->placeholders[$name])) {
            return $this->placeholders[$name];
        }
        return NULL;
    }

}
