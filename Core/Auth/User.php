<?php

namespace Engine5\Core\Auth;

/**
 * Description of User
 *
 * @author barcis
 */
class User {

    private $username;
    private $groups = [];

    public function __construct($username, $groups = ['Public']) {
        $this->username = $username;

        $rc = new \ReflectionClass(\Engine5\Core\Engine::getCurrentAppClassName());

        $namespace = $rc->getNamespaceName() . '\\Acl\\';

        if (is_array($groups)) {
            foreach ($groups as $groupName) {
                if ($groupName === 'Public') {
                    $this->groups['Public'] = new Group('Public');
                } else {
                    $className = $namespace . $groupName;
                    if (class_exists($className)) {
                        $this->groups[$groupName] = new $className($groupName);
                    }
                }
            }
        }
    }

    public function getUsername() {
        return $this->username;
    }

    public function getGroups() {
        return $this->groups;
    }

    public function getGroup($name) {
        return isset($this->groups[$name]) ? $this->groups[$name] : null;
    }

    public function hasGroup($name) {
        if ($name === 'Public') {
            return true;
        }

        if (isset($this->groups[$name])) {
            return true;
        }
        foreach ($this->groups as $group) {
            if ($group instanceof $name) {
                return true;
            }
        }

        return false;
    }

    public function hasGroups($names) {
        foreach ($names as $name) {
            if ($this->hasGroup($name) === true) {
                return true;
            }
        }
        return false;
    }

}
