<?php

namespace Engine5\Core\Auth;

/**
 * Description of Group
 *
 * @author barcis
 */
class Group {

    protected $displayName;
    private $name;

    public final function __construct($name) {
        $this->name = $name;
    }

    public final function getName() {
        return $this->name;
    }

    public final function getDisplayName() {
        return ($this->displayName) ? $this->displayName : $this->name;
    }

}
