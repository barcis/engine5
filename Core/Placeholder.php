<?php

namespace Engine5\Core;

/**
 * Description of Placeholder
 *
 * @author barcis
 */
class Placeholder implements \ArrayAccess {

    private $use;

    /**
     * @var Regions
     */
    private $regions;
    private $name;
    private $templater;

    /**
     * @var bool
     */
    private $used;

    /**
     * @param string $name
     */
    public function __construct($name, \Engine5\Interfaces\Templater &$templater) {
        $this->use = FALSE;
        $this->name = $name;
        $this->regions = new Regions();
        $this->templater = $templater;
    }

    /**
     * @return bool
     */
    public function used() {
        return $this->used;
    }

    public function insert($key, $region, $orderby) {
        return $this->regions->insert($key, $region, $orderby);
    }

    public function regions() {
        return $this->regions->regions();
    }

    /**
     * @return string
     */
    public function renderRegion($runMode, $class, $view, $params = array()) {

        $id = 'L_' . $class . '__' . $view . '__' . md5(@\implode('', $params));
        if ($runMode == \Engine5\Interfaces\Templater::RUN_MODE_DRY ||
                ($runMode == \Engine5\Interfaces\Templater::RUN_MODE_WET &&
                Context::currentContext()->name == 'region')
        ) {
            $controllerViewArgs = Engine::getCurrentApp()->getControllerViewArgs();
            Context::setContext(Context::CONTEXT_REGION, $class);
            $rfc = new \ReflectionClass($class);

            $this->regions[$id] = $rfc->newInstance($rfc->name, $view, $this->templater, $params, $controllerViewArgs);
            Context::unsetContext();
        }
        return $this->regions->renderRegion($runMode, $id);
    }

    /**
     * @return string
     */
    public function render($runMode) {
        return $this->regions->render($runMode);
    }

    public function offsetExists($offset) {
        return isset($this->regions[$offset]);
    }

    public function offsetGet($offset) {
        return $this->regions[$offset];
    }

    /**
     * @param string $offset
     * @param Placeholder $value
     */
    public function offsetSet($offset, $value) {
        $this->regions[$offset] = $value;
    }

    public function offsetUnset($offset) {
        unset($this->regions[$offset]);
    }

}
