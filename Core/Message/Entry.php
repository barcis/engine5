<?php

namespace Engine5\Core\Message;

/**
 * Description of Entry
 *
 * @author barcis
 */
class Entry {

    /**
     * @var string
     */
    public $message;

    /**
     * @var string
     */
    public $sender;

    public function __construct($message, $sender) {
        $this->message = $message;
        $this->sender = $sender;
    }

}
