<?php

namespace Engine5\Core\Session;

/**
 * Description of Standard
 *
 * @author barcis
 */
class Standard implements \Engine5\Interfaces\Session {

    /**
     * @var \Engine5\Interfaces\Session
     */
    private static $instance = null;
    private $session = array();

    public static function free() {
        if (!is_null(self::$instance)) {
            self::$instance->__destruct();
        }
        session_write_close();
    }

    private function __construct() {
        $domain = \Engine5\Core\Engine::getCurrentAppDomain();
        $domain = '.' . str_replace('www.', '', $domain);

        $sessionParams = session_get_cookie_params();

        $sessionParams['domain'] = $domain;

        session_set_cookie_params($sessionParams['lifetime'], $sessionParams['path'], $sessionParams['domain'], $sessionParams['secure'], $sessionParams['httponly']);

        session_start();
        $this->session = $_SESSION;
        //$_SESSION = 'empty';
    }

    public function __destruct() {
        if (!is_null(self::$instance)) {
            $_SESSION = $this->session;
            self::$instance = null;
        }
    }

    public static function getInstance() {
        if (!is_null(self::$instance)) {
            throw new Exception('redeclare session');
        }
        self::$instance = new Standard();
        return self::$instance;
    }

    public function get($field) {
        if (!isset($this->session[$field])) {
            return null;
        }
        return $this->session[$field];
    }

    public function set($field, $value) {
        $this->session[$field] = $value;
    }

    public function exists($field) {
        return isset($this->session[$field]);
    }

}
