<?php

namespace Engine5\Core\Engine;

/**
 * Description of config
 *
 * @author barcis
 *
 * @property-read $applications array
 * @property-read $configs array
 * @property-read $currentAppConfig array
 * @property-read $roles array
 * @property-read $defaultRole string
 */
final class Config extends \Engine5\Pattern\Getter {

    /**
     * @var Config
     */
    private static $_instance;

    public function __get($name) {
        if ($name === 'currentAppConfig') {
            $app = \Engine5\Core\Engine::getCurrentAppName();
            $tmp = parent::__get('configs');
            return $tmp[$app];
        }
        return parent::__get($name);
    }

    /**
     * @return Config
     */
    static public function getInstance() {
        if (!(self::$_instance instanceof Config)) {
            self::$_instance = new Config('/apps/config.yml');
        }
        return self::$_instance;
    }

    protected function __construct($cfg) {
        $file = ST_ROOTDIR . $cfg;
        if (!file_exists($file)) {

            throw new \Exception("<b>fatal error:</b> config file '{$file}' not exists");
        }

        parent::__construct(\Engine5\Tools\Yaml::parseFile($file));
    }

}
