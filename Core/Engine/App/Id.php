<?php

namespace Engine5\Core\Engine\App;

use \Engine5\Core\Context as Context;

/**
 * Description of appid
 *
 * @author barcis
 *
 * @property-read $name
 * @property-read $domain
 * @property-read $basepath
 * @property-read $pathinfo
 */
class Id extends \Engine5\Pattern\Getter {

    private $_app = null;

    public function __construct(array $attr) {
        $params = array();
        $patern = preg_replace('/^\*/', '(?<subdomain>[a-zA-Z0-9\-\.]+)', $attr['patern']);
        preg_match("%^{$patern}%", $attr['domain'], $params);
        $paramsName = array_fill_keys(preg_grep('/^[0-9]+$/', array_keys($params), PREG_GREP_INVERT), 0);
        $attr['params'] = array_intersect_key($params, $paramsName);

        parent::__construct($attr);
    }

    public function getAppClassName() {
        return \Engine5\Core\Engine::getConfig()->currentAppConfig['invoker'];
    }

    private function getRequestPathInfo() {
        return $this->__get('pathinfo');
    }

    private function getRequestUri() {
        return $this->__get('uri');
    }

    private function getRequestMethod() {
        return $this->__get('method');
    }

    /**
     * @return \Engine5\Interfaces\App
     */
    public function getApp() {
        if (!($this->_app instanceof \Engine5\Interfaces\App)) {
            $name = $this->getAppClassName();
            Context::setContext(Context::CONTEXT_APPLICATION, $name);
            $rfc = new \ReflectionClass($name);

            $this->_app = $rfc->newInstance($this->getRequestPathInfo(), $this->getRequestMethod(), $this->getRequestUri());
            Context::unsetContext();
        }

        return $this->_app;
    }

}
