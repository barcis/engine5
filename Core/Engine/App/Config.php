<?php

namespace Engine5\Core\Engine\App;

/**
 * Description of Config
 *
 * @author barcis
 * @property-read $type
 * @property-read $host
 * @property-read $name
 * @property-read $user
 * @property-read $pass
 * @property-read $port
 * @property-read $prefix
 */
class Config implements \Engine5\Interfaces\App\Config {

    private $config;

    public function __construct(\Engine5\Interfaces\App\Config $cfg) {
        $this->config = $cfg;
    }

    public function __get($name) {
        if (isset($this->config->{$name})) {
            return $this->config->{$name};
        }
    }

    protected function set($name, $value) {
        $this->config->{$name} = $value;
    }

}
