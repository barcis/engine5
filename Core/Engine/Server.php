<?php

namespace Engine5\Core\Engine;

/**
 * Description of Server
 *
 * @author barcis
 */
class Server {

    /**
     * @var Server
     */
    private static $instance = null;

    /**
     * @var array
     */
    private $_data;

    private function __construct() {
        $this->_data = $_SERVER;
        //$_SERVER = ['empty'];
        if (!isset($this->_data['REDIRECT_URL']) && isset($this->_data['DOCUMENT_URI'])) {
            $this->_data['REDIRECT_URL'] = $this->_data['DOCUMENT_URI'];
        }
    }

    public static function getInstance() {
        if (!is_null(self::$instance)) {
            throw new \Exception('redeclare request');
        }
        self::$instance = new Server();
        return self::$instance;
    }

    final public function __isset($name) {
        return isset($this->_data[$name]);
    }

    final public function getClientIP() {
        if (isset($this->_data['REMOTE_ADDR'])) {
            return $this->_data['REMOTE_ADDR'];
        } else {
            return '0.0.0.0';
        }
    }

    public function __get($name) {
        if (isset($this->_data[$name])) {
            return $this->_data[$name];
        }
        throw new \Engine5\Exception\Getter("Podany index '{$name}' nie istnieje!");
    }

}
