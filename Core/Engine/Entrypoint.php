<?php

namespace Engine5\Core\Engine;

/**
 * Description of entrypoint
 *
 * @author barcis
 */
abstract class Entrypoint {

    const WWW = 'www';
    const AJAX = 'ajax';
    const VIEW = 'view';
    const REST = 'rest';
    const SCRIPT = 'script';
    const CRON = 'cron';

}
