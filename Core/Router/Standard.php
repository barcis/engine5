<?php

namespace Engine5\Core\Router;

use \Engine5\Core\Engine;
use \Engine5\Core\Request;
use \Engine5\Tools\Yaml;
use \Engine5\Core\ControllerViewArgs;
use \Engine5\Core\Engine\Entrypoint;

/**
 * Description of Standard
 *
 * @author barcis
 */
class Standard implements \Engine5\Interfaces\Router {

    private $config = array();
    private $urls = array();
    private $modifiers = array();

    public function __construct() {
        $this->config = Yaml::parseFile(ST_CONFIGDIR . '/router.config.yml');

        if (!isset($this->config['controller'])) {
            $this->config['controller'] = '\\Engine5\\Core\\Controller';
        }

        if (!isset($this->config['theme'])) {
            $this->config['theme'] = 'default';
        }

        if (!isset($this->config['modifiers'])) {
            $this->config['modifiers'] = [0 => []];
        }

        $this->modifiers['global'] = $this->config['modifiers'];

        if (isset($this->config['urls']) && is_array($this->config['urls'])) {
            foreach ($this->config['urls'] as $action => $_urls) {
                if (isset($_urls['links'])) {
                    $urls = [];
                    foreach ($_urls['links'] as $_url) {
                        if (is_array($_url)) {
                            $u = array_keys($_url)[0];
                            $urls[] = $u;
                            $this->modifiers[$u] = $_url[$u][0];
                        } else {
                            $urls[] = $_url;
                        }
                    }

                    $this->urls = array_merge($this->urls, array_fill_keys($urls, [
                        'theme' => $_urls['theme'],
                        'action' => $action
                    ]));
                }
            }
        }
    }

    private $cache = [];

    public function translateFromUrl($url, $method = null, $app = null, $uri = null) {

        $_id = md5($url . $method . $app . $uri);

        if (isset($this->cache[$_id])) {
            return $this->cache[$_id];
        }




        if (Engine::getEntryPoint() != Entrypoint::AJAX) {
            $url = strtolower(urldecode($url));
        }
        if (!$uri) {
            $uri = $url;
        }
        $bases = $this->getBaseForAplication($app);
        $urls = $bases['reversurls'];

        $params = array();
        foreach ($urls as $patternUrl => $config) {
            $test = $config['uri'] ? $uri : $url;
            $query = Engine::getQueryString();
            if (isset($config['query']) && $config['query'] && $query) {
                $test = str_replace('?' . $query, '', $test);
            }

            if (preg_match("%^$patternUrl$%", $test, $params)) {

                $paramsName = array_fill_keys(preg_grep('/^[0-9]+$/', array_keys($params), PREG_GREP_INVERT), 0);
                $paramList = array_intersect_key($params, $paramsName);


                foreach ($paramList as $param => $value) {
                    if (isset($this->modifiers['global'][$param])) {
                        foreach ($this->modifiers['global'][$param] as $modifier) {
                            $paramList[$param] = call_user_func_array($modifier, ['from', $paramList[$param], &$paramList]);
                        }
                    }
                }

                if (isset($this->modifiers[$patternUrl])) {
                    foreach ($paramList as $param => $value) {
                        if (isset($this->modifiers[$patternUrl][$param])) {
                            foreach ($this->modifiers[$patternUrl][$param] as $modifier) {
                                $paramList[$param] = call_user_func_array($modifier, [$paramList[$param], &$paramList]);
                            }
                        }
                    }
                }
                if (Engine::getEntryPoint() === Entrypoint::AJAX ||
                        Engine::getEntryPoint() === Entrypoint::VIEW) {
                    $paramList = array_merge($paramList, Request::getAjaxParams());
                }
                $this->cache[$_id] = new ControllerViewArgs($this->config['controller'], $config['action'], $paramList, $config['theme']);

                return $this->cache[$_id];
            }
        }

        throw new \Exception('Nie znaleźono strony!');
    }

    public function getBaseForAplication($app = null) {
        $base = '';

        $currentAppname = Engine::getCurrentAppName();
        if (empty($app)) {
            $urls = $this->config ['urls'];
            $base = Engine::getCurrentAppProto() .
                    '://' .
                    Engine::getCurrentAppDomain() .
                    Engine::getCurrentAppPort() .
                    Engine::getCurrentAppBaseUrl();
        } else {
            $configs = Engine::getConfig()->configs;
            if (!isset($configs[$app])) {
                throw new \Exception('Application "' . $app . '" does not exists!');
            }
            $config = $configs[$app];

            if (!isset($config['defaultDomain'])) {
                throw new \Exception('Application "' . $app . '" has not configured the default domain!');
            }
            $base = $config['defaultDomain'];
            if (strpos($base, 'http') !== 0) {
                $base = 'http://' . $base;
            }
            if (substr($base, -1) != '/') {
                $base .= '/';
            }
            //ważne - tutaj musi zostać ST_ROOTDIR zamiast ST_CONFIGDIR

            $arr = Yaml::parseFile(ST_ROOTDIR . '/apps/content/' . $app . '/config/router.config.yml');
            $urls = $arr['urls'];

            unset($arr);
        }

        $reversurls = [];
        if (isset($urls) && is_array($urls)) {
            foreach ($urls as $action => $_urls) {
                if (isset($_urls['links'])) {
                    $Urls = [];
                    foreach ($_urls['links'] as $_url) {
                        if (is_array($_url)) {
                            $u = array_keys($_url)[0];
                            $Urls[] = $u;
                            $this->modifiers[$u] = $_url[$u][0];
                        } else {
                            $Urls[] = $_url;
                        }
                    }
                    $reversurls = array_merge($reversurls, array_fill_keys($Urls, [
                        'theme' => $_urls['theme'],
                        'uri' => isset($_urls['uri']) ? $_urls['uri'] === true : false,
                        'action' => $action
                    ]));
                }
            }
        }
        return ['base' => $base, 'urls' => $urls, 'reversurls' => $reversurls];
    }

    public function translateToUrl(ControllerViewArgs $controllerViewArgs, $withBase = true) {
        $target = $controllerViewArgs->action;
        $bases = $this->getBaseForAplication(!empty($controllerViewArgs->app) ? $controllerViewArgs->app : null);


        $base = $withBase ? $bases['base'] : '';
        $urls = $bases['urls'];

        if (!empty($urls[$target])) {
            $params = $controllerViewArgs->params;
            foreach ($params as $param => $value) {
                if (isset($this->modifiers['global'][$param])) {
                    foreach ($this->modifiers['global'][$param] as $modifier) {
                        $val = call_user_func_array($modifier, ['to', $params[$param], &$params]);
                        if ($val === null) {
                            unset($params[$param]);
                        } else {
                            $params[$param] = $val;
                        }
                    }
                }
            }
            $paramsCount = count($params);


            foreach ($urls[$target]['links'] as $u) {
                preg_match_all('`
                     \\(\\?P?<([a-zęóśłżźćńA-ZĘÓŚŁŻŹĆŃ0-9_]+?)>       # (?<nazwa>
                    (?:                            # /--
                        (?>[^()[\\]]*)             #       zero lub więcej znaków bez [] i ()
                        |                          #       lub
                        \\((?!\\?)                 #       ( bez ? po nim
                        (?:                        #       /--
                            (?>[^()[\\]]+)         #               pewna ilośc znaków bez [] i ()
                            |                      #               lub
                            (?R)                   #               rekursywnie cały wzorzec
                        )*                         #       \-- powtórzone zero lub więcej razy
                        \\)                        #       )
                        |                          #       lub
                        (?R)                       #       rekursywnie cały wzorzec
                    )*                             # \-- powtórzone zero lub więcej razy
                    \\)                            # )
                    |                              # lub
                    \\[                            # [
                        (?:                        # /--
                            (?>[^[\\]]+)           #       zero lub więcej znaków bez []
                            |                      # lub
                            (?R)                   #       rekursywnie cały wzorzec
                        )*                         # \-- powtórzone zero lub więcej razy
                    \\]
                                `ixs', $u, $matches, PREG_SET_ORDER | PREG_OFFSET_CAPTURE);
                if (count($matches) == $paramsCount) {
                    $tmpP = array();
                    foreach ($matches as $mm) {
                        if (isset($mm[1][0])) {
                            //pd([$controllerViewArgs, $mm]);
                            $tmpP[] = $mm[1][0];
                        }
                    }

                    if (count(array_diff($tmpP, array_keys($params))) > 0) {
                        continue;
                    }

                    foreach ($matches as $m) {
                        if (isset($m[1][0]) && isset($params[$m[1][0]])) {
                            $keys[] = $m[0][0];
                            $repl[] = $params[$m[1][0]];
                        } else {
                            break;
                        }
                    }
                    $keys[] = '.*';
                    $keys[] = '.+';
                    $repl[] = '';
                    $repl[] = '';

                    return (
                            $base .
                            ltrim(preg_replace('`[^a-zęóśłżźćń0-9\/\-\.\\,]`', '', str_replace($keys, $repl, $u)), '/')
                            );
                }
            }
        }
    }

}
