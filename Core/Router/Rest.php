<?php

namespace Engine5\Core\Router;

use \Engine5\Core\Engine;
use \Engine5\Core\Request;
use \Engine5\Tools\Yaml;
use \Engine5\Core\ControllerViewArgs;

/**
 * Description of Rest
 *
 * @author barcis
 */
class Rest implements \Engine5\Interfaces\Router {

    private $config = ['files' => [], 'urls' => []];
    private $urls = [];

    public function __construct() {
        $appClass = Engine::getCurrentAppClassName();
        $rc = new \ReflectionClass($appClass);

        while ($rc && $rc->getParentClass() !== null) {
            $configFileName = dirname($rc->getFileName()) . '/config/rest.config.yml';
            $this->config['files'][] = $configFileName;
            if (file_exists($configFileName)) {
                $config = Yaml::parseFile($configFileName);

                if (!isset($this->config['controller']) && isset($config['controller'])) {
                    $this->config['controller'] = $config['controller'];
                }

                if (isset($config['urls']) && is_array($config['urls'])) {
                    $this->config['urls'] = array_merge($this->config['urls'], $config['urls']);
                }
            }
            $rc = $rc->getParentClass();
        }



        if (!isset($this->config['controller'])) {
            $this->config['controller'] = '\\Engine5\\Core\\Controller\\Rest';
        }

        //plugins
        $AppName = \Engine5\Core\Engine::getCurrentAppName();
        $AppConfig = \Engine5\Core\Engine::getConfig()->configs[$AppName];

        if (isset($AppConfig['plugins'])) {
            foreach ($AppConfig['plugins'] as $plugin) {
                if (!isset($plugin['class'])) {
                    break;
                }
                $pluginClassName = $plugin['class'];
                $rc = new \ReflectionClass($pluginClassName);
                $configDir = dirname($rc->getFileName()) . '/config';
                $configFile = $configDir . '/rest.config.yml';
                if (!file_exists($configDir) || !file_exists($configFile)) {
                    break;
                }

                $pluginConfig = Yaml::parseFile($configFile);

                $this->config['urls'] = array_merge($this->config['urls'], $pluginConfig['urls']);
            }
        }

        if (isset($this->config['urls']) && is_array($this->config['urls'])) {
            $this->urls = $this->config['urls'];
        }
    }

    public function getBaseForAplication($app = null) {
        $base = '';

        $currentAppname = Engine::getCurrentAppName();
        if (empty($app) || $app == $currentAppname) {
            $urls = $this->config ['urls'];
            $base = Engine::getCurrentAppProto() .
                    '://' .
                    Engine::getCurrentAppDomain() .
                    Engine::getCurrentAppPort() .
                    Engine::getCurrentAppBaseUrl();
        } else {
            $configs = Engine::getConfig()->configs;
            if (!isset($configs[$app])) {
                throw new Exception('Application "' . $app . '" does not exists!');
            }
            $config = $configs[$app];

            if (!isset($config['defaultDomain'])) {
                throw new Exception('Application "' . $app . '" has not configured the default domain!');
            }
            $base = $config['defaultDomain'];
            if (strpos($base, 'http') !== 0) {
                $base = 'http://' . $base;
            }
            if (substr($base, -1) != '/') {
                $base .= '/';
            }
            $arr = Yaml::parseFile(ST_CONFIGDIR . '/router.config.yml');
            $urls = $arr['urls'];

            unset($arr);
        }
        return ['base' => $base, 'urls' => $urls];
    }

    public function translateFromUrl($url, $method = null, $app = null, $uri = null) {
        $_method = strtolower($method);


        $params = array();
        foreach ($this->urls as $patternUrl => $methods) {
            if (isset($methods[$_method]) && preg_match("%^$patternUrl$%", $url, $params)) {
                $action = is_array($methods[$_method]) && isset($methods[$_method]['action']) ? $methods[$_method]['action'] : $methods[$_method];
                $paramsName = array_fill_keys(preg_grep('/^[0-9]+$/', array_keys($params), PREG_GREP_INVERT), 0);
                $paramList = array_merge(array_intersect_key($params, $paramsName), Request::getAjaxParams());

                $cache = is_array($methods[$_method]) && isset($methods[$_method]['cache']) ? (bool) $methods[$_method]['cache'] : false;
                $acl = is_array($methods[$_method]) && isset($methods[$_method]['acl']) ? $methods[$_method]['acl'] : true;
                $login = is_array($methods[$_method]) && isset($methods[$_method]['login']) ? (bool) $methods[$_method]['login'] : false;

                $cva = new ControllerViewArgs($this->config['controller'], $action, $paramList, null, '', $cache, ['acl' => $acl, 'login' => $login]);

                return $cva;
            }
        }

        http_response_code(404);
        throw new \Exception('Nie obsługiwane zapytanie!');
    }

    public function translateToUrl(ControllerViewArgs $controllerViewArgs) {
        return '';
    }

}
