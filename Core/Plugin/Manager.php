<?php

namespace Engine5\Core\Plugin;

/**
 * Description of Manager
 *
 * @author barcis
 */
final class Manager {

    /**
     * @var Manager
     */
    private static $instance;
    private static $plugins;
    private $hooks = [];

    /**
     * @throws \Exception
     */
    public static function create() {
        if (!is_null(self::$instance)) {
            throw new \Exception('recreate ' . __CLASS__);
        }
        self::$instance = new Manager();
    }

    /**
     *
     * @return Manager
     * @throws \Exception
     */
    public static function getInstance() {
        if (is_null(self::$instance)) {
            throw new \Exception('nie utworzono jeszcze pluginsManagera');
        }

        return self::$instance;
    }

    public static function addRegionToHook($hook, $region) {
        if (!isset(self::getInstance()->hooks[$hook])) {
            self::getInstance()->hooks[$hook] = [];
        }

        self::getInstance()->hooks[$hook][] = $region;
    }

    public static function getRegionsForHook($hook) {
        if (!isset(self::getInstance()->hooks[$hook])) {
            return [];
        }

        return self::getInstance()->hooks[$hook];
    }

    private function __construct() {

    }

}
