<?php

namespace Engine5\Core;

/**
 * Description of Auth
 *
 * @author barcis
 */
class Auth {

    /**
     * @var Auth
     */
    private static $_instance;
    private $prefix = 'default';

    /**
     * @var \Engine5\Interfaces\Session
     */
    private $_session;

    /**
     * @throws Exception
     */
    public static function create(\Engine5\Interfaces\Session &$session) {
        if (!is_null(self::$_instance)) {
            throw new \Exception('recreate ' . __CLASS__);
        }
        self::$_instance = new Auth($session);
    }

    public function __construct(\Engine5\Interfaces\Session &$session) {
        $this->_session = $session;
    }

    public static function setProfix($prefix) {
        self::$_instance->prefix = $prefix;
    }

    public static function getProfix() {
        return self::$_instance->prefix;
    }

    public static function login(\Engine5\Core\Auth\User $user, \SORM\Model $muser = null, $prefix = null) {

        if (!$prefix) {
            $prefix = self::$_instance->prefix;
        }

        self::$_instance->_session->set($prefix . '_currentUser', $user);
        self::$_instance->_session->set($prefix . '_currentUserModel', $muser);
    }

    public static function logout($prefix = null) {
        if (!$prefix) {
            $prefix = self::$_instance->prefix;
        }

        self::$_instance->_session->set($prefix . '_currentUser', null);
        self::$_instance->_session->set($prefix . '_currentUserModel', null);
    }

    public static function isLogged($prefix = null) {
        if (is_null(self::$_instance)) {
            return false;
        }

        if (!$prefix) {
            $prefix = self::$_instance->prefix;
        }

        if (!isset(self::$_instance->_session) || !is_object(self::$_instance->_session)) {
            return false;
        }

//        if (self::$_instance->_session->get($prefix . '_currentUserModel') === null) {
//            return false;
//        }

        if (self::$_instance->_session->get($prefix . '_currentUser') === null) {
            return false;
        }

        return true;
    }

    public static function reload($prefix = null, $fields = ['*']) {
        if (!$prefix) {
            $prefix = self::$_instance->prefix;
        }

        if (self::isLogged($prefix)) {
            $u = self::$_instance->_session->get($prefix . '_currentUserModel');

            if ($u) {
                $newuser = $u::q()
                        ->where('id', $u->id)
                        ->fields($fields)
                        ->one();

                if ($newuser) {
                    self::$_instance->_session->set($prefix . '_currentUserModel', $newuser);
                    return $newuser;
                }
            }
        }
        return false;
    }

    /**
     * @return \Engine5\Core\Auth\User|\SORM\Model
     */
    public static function getLoggedUser($model = false, $prefix = null) {
        if (!$prefix) {
            $prefix = self::$_instance->prefix;
        }
        if ($model) {
            return self::$_instance->_session->get($prefix . '_currentUserModel');
        }

        return self::$_instance->_session->get($prefix . '_currentUser');
    }

    public static function getLoggedUserId($prefix = null) {
        if (!$prefix) {
            $prefix = self::$_instance->prefix;
        }
        $model = self::getLoggedUser(true, $prefix);

        if ($model) {
            return (int) $model->id;
        }
        return null;
    }

}
