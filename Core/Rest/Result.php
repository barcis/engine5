<?php

namespace Engine5\Core\Rest;

/**
 * Description of Result
 *
 * @author barcis
 */
class Result {

    private $result = array();
    private $code = 200;
    private $text = 'Other';

    public function __construct($result, $code = 200, $text = 'Other') {
        $this->result = $result;
        $this->code = (int) $code;
        $this->text = $text;
    }

    public function getResult() {
        return $this->result;
    }

    public function getCode() {
        return $this->code;
    }

    public function getJSON() {
        header('Content-Type: application/json; charset=UTF-8');
        if (is_object($this->result)) {
            header('E5-Object: ' . get_class($this->result));
            if ($this->result instanceof \SORM\Models) {
                header('E5-sql: ' . preg_replace('/\n/', ' ', $this->result->getSql()));
            }
        }
        return json_encode($this->result, JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_UNESCAPED_UNICODE);
    }

    public function sendCode($cacheConfig, $cacheEnabled) {
        switch ($this->code) {
            case 100: $text = 'Continue';
                break;
            case 101: $text = 'Switching Protocols';
                break;
            case 200: $text = 'OK';
                break;
            case 201: $text = 'Created';
                break;
            case 202: $text = 'Accepted';
                break;
            case 203: $text = 'Non-Authoritative Information';
                break;
            case 204: $text = 'No Content';
                break;
            case 205: $text = 'Reset Content';
                break;
            case 206: $text = 'Partial Content';
                break;
            case 300: $text = 'Multiple Choices';
                break;
            case 301: $text = 'Moved Permanently';
                break;
            case 302: $text = 'Moved Temporarily';
                break;
            case 303: $text = 'See Other';
                break;
            case 304: $text = 'Not Modified';
                break;
            case 305: $text = 'Use Proxy';
                break;
            case 400: $text = 'Bad Request';
                break;
            case 401: $text = 'Unauthorized';
                break;
            case 402: $text = 'Payment Required';
                break;
            case 403: $text = 'Forbidden';
                break;
            case 404: $text = 'Not Found';
                break;
            case 405: $text = 'Method Not Allowed';
                break;
            case 406: $text = 'Not Acceptable';
                break;
            case 407: $text = 'Proxy Authentication Required';
                break;
            case 408: $text = 'Request Time-out';
                break;
            case 409: $text = 'Conflict';
                break;
            case 410: $text = 'Gone';
                break;
            case 411: $text = 'Length Required';
                break;
            case 412: $text = 'Precondition Failed';
                break;
            case 413: $text = 'Request Entity Too Large';
                break;
            case 414: $text = 'Request-URI Too Large';
                break;
            case 415: $text = 'Unsupported Media Type';
                break;
            case 500: $text = 'Internal Server Error';
                break;
            case 501: $text = 'Not Implemented';
                break;
            case 502: $text = 'Bad Gateway';
                break;
            case 503: $text = 'Service Unavailable';
                break;
            case 504: $text = 'Gateway Time-out';
                break;
            case 505: $text = 'HTTP Version not supported';
                break;
            default:
                $text = preg_replace('/([A-Z])/', ' ${1}', $this->text);
                break;
        }
        if ((is_array($cacheConfig) && isset($cacheConfig['created']) && $cacheConfig['created'] > 0)) {
            $cache_create_date = gmdate('D, d M Y H:i:s', $cacheConfig['created']);
            $cache_expire_date = gmdate('D, d M Y H:i:s', $cacheConfig['expires']);
            header('E5-CacheCreated: ' . $cache_create_date . ' GMT');
            header('E5-CacheExpires: ' . $cache_expire_date . ' GMT');
            header('Expires: ' . $cache_expire_date . ' GMT');
            header("Pragma: cache");
            header('Cache-Control: public, max-age=' . $cacheConfig['timeout']);
            header('User-Cache-Control: max-age=' . $cacheConfig['timeout']);
        }
        header('E5-Cache: ' . var_export(is_array($cacheConfig) && ($cacheConfig['cached'] === true), true));
        header('HTTP/1.1 ' . $this->code . ' ' . $text);

        $GLOBALS['http_response_code'] = $this->code;

        return $this->code;
    }

}
