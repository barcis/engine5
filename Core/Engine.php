<?php

namespace Engine5\Core;

/**
 * Description of Engine
 *
 * @author barcis
 */
class Engine {

    /**
     * @var Engine\App\Id
     */
    private $_appid;

    /**
     * @var Engine5\Core\Engine\Config
     */
    private $_config;

    /**
     * @var string
     */
    private $_mode;

    /**
     * @var Engine\Server
     */
    private $_server;

    /**
     * @var \Engine5\Interfaces\Session
     */
    private $_session;

    /**
     * @var Engine
     */
    static private $_instance;

    /**
     * @return string
     */
    static public function getCurrentAppDomainPatern() {
        return self::$_instance->_appid->patern;
    }

    /**
     * @return string
     */
    static public function getCurrentAppDomainParams() {
        return self::$_instance->_appid->params;
    }

    /**
     * @return App
     */
    static public function getCurrentApp() {
        return self::$_instance->_appid->getApp();
    }

    /**
     * @return string
     */
    static public function getCurrentAppName() {
        return self::$_instance->_appid->name;
    }

    /**
     * @return string
     */
    static public function getCurrentAppBaseUrl() {
        return self::$_instance->_appid->basepath;
    }

    /**
     * @return string
     */
    static public function getCurrentAppClassName() {
        return self::$_instance->_appid->getAppClassName();
    }

    /**
     * @return string
     */
    static public function getCurrentAppPort() {

        $defaultPort = self::$_instance->getCurrentAppProto() === 'https' ? 443 : 80;

        if (self::$_instance->_server->SERVER_PORT != $defaultPort) {
            return ':' . self::$_instance->_server->SERVER_PORT;
        }
        return '';
    }

    static public function getCurrentAppProto() {
        if (isset(self::$_instance->_server->HTTPS) && self::$_instance->_server->HTTPS != 'off') {
            return 'https';
        }
        return 'http';
    }

    static public function getCurrentAppDomain() {
        return self::$_instance->_appid->domain;
    }

    static public function getCurrentAppDefaultDomain() {
        return self::getConfig()->currentAppConfig['defaultDomain'];
    }

    /**
     * @return Engine\Config
     */
    static public function getConfig() {
        return self::$_instance->_config;
    }

    public static function getEntryPoint() {
        if (self::$_instance instanceof Engine) {
            return self::$_instance->_mode;
        }
    }

    public static function hasPlugin($name) {
        if (
                !isset(self::$_instance->_config->configs[self::$_instance->_appid->name]) ||
                !isset(self::$_instance->_config->configs[self::$_instance->_appid->name]['plugins']) ||
                !isset(self::$_instance->_config->configs[self::$_instance->_appid->name]['plugins'][$name])
        ) {
            return false;
        }
        return true;
    }

    /**
     * @return Engine
     */
    static public function getInstance($mode = null, $server = null) {
        if (!empty($mode) && !(self::$_instance instanceof Engine)) {
            if ($mode == Engine\Entrypoint::WWW ||
                    $mode == Engine\Entrypoint::AJAX ||
                    $mode == Engine\Entrypoint::VIEW ||
                    $mode == Engine\Entrypoint::REST ||
                    $mode == Engine\Entrypoint::SCRIPT ||
                    $mode == Engine\Entrypoint::CRON) {
                try {
                    self::$_instance = new Engine();
                    self::$_instance->init($mode, $server);
                } catch (\Exception $e) {
                    http_response_code(404);
                    die('<div style="color:red; font-weight:bold;">Engin5 exception</div>' . $e->getMessage());
                }
            } else {
                throw new \Exception('fatal error: unknown entry point');
            }
        } else {
            throw new \Exception('fatal error');
        }
        return self::$_instance;
    }

    public static function getAcceptLanguage() {
        $values = explode(',', self::$_instance->_server->HTTP_ACCEPT_LANGUAGE);

        $accept_language = array();

        foreach ($values AS $lang) {
            $cnt = preg_match('/([-a-zA-Z]+)\s*;\s*q=([0-9\.]+)/', $lang, $matches);
            if ($cnt === 0)
                $accept_language[$lang] = 1;
            else
                $accept_language[$matches[1]] = $matches[2];
        }

        return $accept_language;
    }

    public static function errorHandler($errno, $errstr, $errfile, $errline, $context) {

        if (error_reporting() == 0) {
            return true;
        }

        if ($errno == E_RECOVERABLE_ERROR) { // Scalar Type-Hinting patch.
            $regexp = '/^Argument (\d)+ passed to (.+) must be an instance of (?<hint>.+), (?<given>.+) given/i';

            if (preg_match($regexp, $errstr, $match)) {

                $given = $match['given'];
                $tmp = explode('\\', $match['hint']);
                $hint = end($tmp); // namespaces support.

                if ($hint == $given)
                    return true;
            }
        } elseif ($errno == 8 && isset($context['_smarty_tpl'])) {
            $_context = (array) $context['_smarty_tpl'];

            if (isset($_context['compiled']->filepath)) {
                $f = file($_context['compiled']->filepath);
                $errstr = $f[$errline - 1] . '<br/>' . $_context['compiled']->source->filepath . '<br/>' . $errstr;
            }
        } elseif ($errno == 8 && preg_match('/smarty_internal_debug.php$/', $errfile) && preg_match('/Undefined index: (start_time|start_template_time)/', $errstr)) {
            return true;
        }

        $text = $errstr;
        $text .= ' (#' . $errno . ')';
        $text .= '<br /><pre>';
//        $text .= print_r(array_reverse($context), true);
        $text .= '</pre><br />';
        $text .= $errfile . '::' . $errline;

        if (isset($context['sql']) && !empty($context)) {
            return true;
        }
        throw new \Exception($text, $errno);
    }

    /**
     * @return Engine\App\Id
     */
    private function identyfyApplication($server = null) {
        if (is_null($server)) {

            $server = $this->_server->SERVER_NAME;
            $method = $this->_server->REQUEST_METHOD;
//            if (isset($this->_server->DOCUMENT_URI)) {
//                $url = $this->_server->DOCUMENT_URI;
//            } else {
//                $url = isset($this->_server->REDIRECT_URL) ? $this->_server->REDIRECT_URL : '/';
//            }
            $url = $uri = isset($this->_server->REQUEST_URI) ? $this->_server->REQUEST_URI : $url;
        } else {
            $uri = $url = '/';
            $method = 'GET';
        }

        if ($this->_server->QUERY_STRING) {
            $url = str_replace('?' . $this->_server->QUERY_STRING, '', $url);
        }

        foreach (array_keys($this->_config->applications) as $_pattern) {
            $pattern = preg_replace('/^\*/', '([a-zA-Z0-9\-]+)', str_replace('.', '\.', $_pattern));

            if (preg_match('%^' . $pattern . '$%', $server)) {
                foreach (array_keys($this->_config->applications[$_pattern]) as $path) {
                    $_path = $path;
                    if ($this->getEntryPoint() === Engine\Entrypoint::REST) {
                        $path .= 'rest/';
                    }
                    if (preg_match('%^' . $path . '%', $url)) {
                        $attr = array(
                            'name' => $this->_config->applications[$_pattern][$_path],
                            'domain' => $server,
                            'method' => $method,
                            'basepath' => $path,
                            'pathinfo' => substr($url, strlen($path)),
                            'uri' => substr($uri, strlen($path)),
                            'patern' => $_pattern
                        );
                        return new Engine\App\Id($attr);
                    }
                }
            }
        }
        throw new \Exception('domena lub scieżka podstawowa nieznana');
    }

    public static function getCurrentLanguage() {
        $lang = self::$_instance->_session->get('current_language');

        if (!$lang) {
            $config = new Engine\App\Config(new Engine\Config\Parser\Yaml('app.config.yml'));
            self::$_instance->_session->set('current_language', $config->defaultLanguage);
            $lang = $config->defaultLanguage;
        }



        return $lang;
    }

    public static function setCurrentLanguage($lang) {
        self::$_instance->_session->set('current_language', $lang);
    }

    public static function getLastUrl() {
        return self::$_instance->_session->get('url_now');
    }

    public static function getCurrentUrl() {
        return self::$_instance->_session->get('url_current');
    }

    public static function getCurrentUri() {
        return self::$_instance->_server->REQUEST_URI;
    }

    private function __construct() {

    }

    public static function getReferer() {
        return self::$_instance->_server->HTTP_REFERER;
    }

    public static function getRequestMethod() {
        return strtolower(self::$_instance->_server->REQUEST_METHOD);
    }

    public static function getQueryString() {
        return (self::$_instance->_server->QUERY_STRING);
    }

    public static function getClientIP() {
        return self::$_instance->_server->getClientIP();
    }

    /**
     *
     * @param string $mode
     */
    private function init($mode, $server = null) {
        $this->_mode = $mode;
        Context::create();
        Context::setContext(Context::CONTEXT_ENGINE, 'engine5');

        if ($this->_mode == Engine\Entrypoint::WWW ||
                $this->_mode == Engine\Entrypoint::AJAX ||
                $this->_mode == Engine\Entrypoint::VIEW ||
                $this->_mode == Engine\Entrypoint::REST) {
            $this->_config = \Engine5\Core\Engine\Config::getInstance();
            header('Content-Type: text/html; charset=UTF-8');

            $this->_server = Engine\Server::getInstance();
            $this->_appid = $this->identyfyApplication();
            $this->_session = \Engine5\Factory\Session::newInstance();

            define('ST_APP', self::getCurrentAppName());

            Plugin\Manager::create();
            Form\Manager::create($this->_mode, $this->_session);
            $uri = isset($this->_server->REDIRECT_URL) ? $this->_server->REDIRECT_URL : '/';
            if ($uri != $this->_session->get('url_now')) {
                Form\Manager::resetAllRequestValues();
            }

            $this->_session->set('url_current', $uri);

            Auth::create($this->_session);
            Resource\Manager::create();
            Fixed::create($this->_session);
            Message::create($this->_session);
        } elseif ($this->_mode == Engine\Entrypoint::SCRIPT || $this->_mode == Engine\Entrypoint::CRON) {
            //$this->_appid = $this->identyfyApplication($server);
            //define('ST_APP', self::getCurrentAppName());
        } else {
            throw new \Exception('Engine5 fatal error: unknown entry point');
        }
    }

    public static function initialize() {
        set_error_handler(array('\\Engine5\\Core\\Engine', 'errorHandler'), E_ALL);

        ini_set('date.timezone', 'Europe/Warsaw');

        if (isset($_SERVER['DOCUMENT_ROOT']) && !empty($_SERVER['DOCUMENT_ROOT'])) {
            define('ST_UPLOADPATH', '/upload/');
            if (!defined('ST_FRONTENDDIR')) {
                define('ST_FRONTENDDIR', realpath($_SERVER['DOCUMENT_ROOT']));
            }
            define('ST_APPDIR', realpath(ST_FRONTENDDIR . '/..'));
            define('ST_UPLOADDIR', realpath(ST_FRONTENDDIR . ST_UPLOADPATH));

            if (!defined('ST_SKINSDIR')) {
                define('ST_SKINSDIR', realpath(ST_APPDIR . '/skins'));
            }
            if (!defined('ST_CONFIGDIR')) {
                define('ST_CONFIGDIR', realpath(ST_APPDIR . '/config'));
            }

            define('ST_ROOTDIR', realpath(ST_APPDIR . '/../../..'));
            define('ST_APPSDIR', realpath(ST_ROOTDIR . '/apps'));
            define('ST_VAR', realpath(ST_ROOTDIR . '/var'));
        }
        $st_debug = false;

        if (file_exists(ST_VAR . '/debug') && isset($_COOKIE['e5dbg'])) {
            $expectValue = trim(file_get_contents(ST_VAR . '/debug'));
            $cookieValue = trim($_COOKIE['e5dbg']);
            $st_debug = ($cookieValue == $expectValue);

            error_reporting(E_ALL);
            ini_set('display_errors', 'on');
        }
        define('ST_DEBUG', $st_debug);


        \Engine5\Cache\Core::instance();

        Debug::beginTimeLogSession('_all');
    }

    public function Run() {

        if (file_exists(ST_ROOTDIR . '/updating.lock')) {
            echo "
                <html>
                <body>
                    <h1>Trwa aktualizacja systemu....</h1>
                </body>
                </html>
                ";
            return;
        }

        try {
            $app = $this->_appid->getApp();
            echo $app->run();
//            die('ssss');
            $this->__die();
        } catch (\Exception $e) {
            http_response_code(404);
            echo "<pre>E5 EXCEPTION\n\n{$e}</pre>";
        }
    }

    public static function destroy() {
        self::$_instance->__die();
    }

    public static function sessionFree() {
        self::$_instance->_session->free();
    }

    public function __die() {
        Message::destroy();
        Fixed::destroy();
        Form\Manager::destroy();
        $uri = isset($this->_server->REDIRECT_URL) ? $this->_server->REDIRECT_URL : '/';

        if ($uri != $this->_session->get('url_now') && $this->getEntryPoint() == Engine\Entrypoint::WWW) {
            $this->_session->set('url_now', $uri);
        }
        Context::unsetContext();
    }

    public static function indexClasses(\Composer\Script\Event $event) {
        //Debug::p($event->getComposer());
    }

    public static function postInstall(\Composer\Script\Event $event) {
        $homePage = $event->getComposer()->getPackage()->getHomepage();
        $domain = parse_url($homePage)['host'];
        $name = preg_replace('/\.lo$/', '', $domain);


        $vendorDir = $event->getComposer()->getConfig()->get('vendor-dir');
        $engineDir = $vendorDir . '/batusystems/engine5/';
        $baseDir = realpath($vendorDir . '/../');
        $varDir = $baseDir . '/var/';
        $cacheDir = $varDir . '/cache/';
        $vhost = '100-' . $name . '.conf';
        $vhostDir = $baseDir . '/' . $vhost;

        if (!file_exists($varDir)) {
            mkdir($varDir, 0777);
            chmod($varDir, 0777);
        }

        if (!file_exists($cacheDir)) {
            mkdir($cacheDir, 0777);
            chmod($cacheDir, 0777);
        }

        $ApacheConfTemplate = file_get_contents($engineDir . 'ApacheConfTemplate.conf');

        $ApacheConf = str_replace('{$defaultDomain$}', $domain, $ApacheConfTemplate);
        $ApacheConf = str_replace('{$baseDir$}', $baseDir, $ApacheConf);

        file_put_contents($vhostDir, $ApacheConf);


        `sudo cp {$vhostDir} /etc/apache2/sites-available/`;
        echo "Plik {$vhostDir} skopiowany do /etc/apache2/sites-available/\n";
        `sudo a2ensite {$vhost}`;
        echo "sudo a2ensite {$vhost}\n";
        `sudo service apache2 reload`;
        echo "sudo service apache2 reload\n";
    }

    public static function createVhost(\Composer\Script\Event $event) {
        Debug::p($event);
    }

}
