<?php

namespace Engine5\Core;

/**
 * Description of App
 *
 * @author barcis
 */
abstract class App implements \Engine5\Interfaces\App {

    /**
     * @var Controller
     */
    protected $controller;

    /**
     * @var \Engine5\Interfaces\Templater
     */
    protected $templater;

    /**
     * @var \Engine5\Core\Engine\App\Config
     */
    protected $config;

    /**
     * @var string
     */
    protected $name;

    /**
     * @var string
     */
    protected $theme;

    /**
     * @var string
     */
    protected $skin;

    /**
     * @var string
     */
    protected $cva;

    public function __construct($pathinfo, $method = null, $uri = null) {
        $this->name = Engine::getCurrentAppName();
        $this->config = new Engine\App\Config(new Engine\Config\Parser\Yaml('app.config.yml'));
        $this->skin = $this->config->defaultSkin;

        $router = \Engine5\Factory\Router::newInstance();
        $this->templater = \Engine5\Factory\Templater::newInstance();

        $appName = Engine::getCurrentAppName();
        $configs = Engine::getConfig()->configs;

//        try {
        if (isset($configs[$appName]) && isset($configs[$appName]['plugins'])) {
            foreach ($configs[$appName]['plugins'] as $plugin) {
                if (!isset($plugin['class']) || !class_exists($plugin['class'])) {
                    continue;
                }
                $rc = new \ReflectionClass($plugin['class']);
                if ($rc->hasMethod('config') && isset($plugin['config'])) {
                    $rc->getMethod('config')->invoke(null, $plugin['config']);
                }
                if ($rc->hasMethod('hooks')) {
                    $rc->getMethod('hooks')->invoke(null);
                }
            }
        }
//        } catch (\Exception $e) {
//
//        }

        try {
            $this->cva = $router->translateFromUrl($pathinfo, $method, $this->name, $uri);
            if (isset($this->cva->theme)) {
                $this->theme = $this->cva->theme;
            }
        } catch (\Exception $e) {
            if (Engine::getEntryPoint() == Engine\Entrypoint::WWW && $this->config->httpErrorMap) {
                $httpErrorMap = (array) $this->config->httpErrorMap;
                if (isset($httpErrorMap[404])) {
                    $newController = $httpErrorMap[404];
                    if ($newController) {
                        $this->cva = new ControllerViewArgs('\\Engine5\\Core\\Controller', $newController);
                        $this->theme = 'default';
                        return;
                    }
                }
            }
            throw $e;
        }
    }

    /**
     *
     * @return ControllerViewArgs
     */
    public function getControllerViewArgs() {
        return $this->cva;
    }

    public function run() {

        $this->controller = $this->cva->getControllerInstance($this->templater);

        if (Engine::getEntryPoint() != Engine\Entrypoint::AJAX && Engine::getEntryPoint() != Engine\Entrypoint::REST) {
            $this->controller->__preInit($this->cva);

            if ($this->controller->hasErrors() && $this->config->httpErrorMap) {
                $httpErrorMap = (array) $this->config->httpErrorMap;
                $errors = $this->controller->getErrors();
                $newController = null;
                foreach ($errors as $error) {
                    if (isset($httpErrorMap[$error['error']])) {
                        $newController = $httpErrorMap[$error['error']];
                        break;
                    }
                }
                if ($newController) {
                    $this->cva = new ControllerViewArgs('\\Engine5\\Core\\Controller', $newController);
                    $this->theme = 'default';
                    $this->controller = $this->cva->getControllerInstance($this->templater);
                    $this->controller->__preInit($this->cva);
                }
            }
        }

        $this->controller->__init();

        if (method_exists($this->controller, $this->cva->action)) {
            call_user_func(array($this->controller, $this->cva->action));
        }

        Context::unsetContext();
        return $this;
    }

    /**
     *
     * @return \Engine5\Core\Engine\App\Config
     */
    public function getConfig() {
        return $this->config;
    }

    /**
     *
     * @return string
     */
    public function getCurrentSkin() {
        return $this->skin;
    }

    public function setLanguageId($id) {
        return $this->config->language = $id;
//        $this->config->setLanguageId($id);
    }

    public function getLanguageId() {
        if (isset($this->config->language)) {
            return $this->config->language;
        }
        return $this->config->defaultLanguage;
    }

    public function getDefaultLanguageId() {
        return $this->config->defaultLanguage;
    }

    public function setLanguageCurrency($code) {
        $this->config->setLanguageCurrency($code);
    }

    public function __toString() {
        try {
            return $this->render();
        } catch (\Exception $e) {
            http_response_code(404);
            echo '<pre>E5APP EXCEPTION\n' . $e->getMessage() . '</pre>';
        }
    }

    private function render() {
        if (Engine::getEntryPoint() == Engine\Entrypoint::REST) {
            return '';
        }
        $dir = ST_SKINSDIR . '/' . $this->skin . '/view/';

        if (!file_exists($dir)) {
            throw new \Exception('Skins directory does not exist!<br/>' . $dir);
        }

        $this->templater->setContainer($this->controller);

        $this->templater->setDryRun(true);
        $html = $this->templater->fetch($this->theme, $dir, $this);

        if ($this->controller->hasErrors() && $this->config->httpErrorMap) {
            $httpErrorMap = (array) $this->config->httpErrorMap;
            $errors = $this->controller->getErrors();
            $newController = null;
            foreach ($errors as $error) {
                if (isset($httpErrorMap[$error['error']])) {
                    $newController = $httpErrorMap[$error['error']];
                    break;
                }
            }
            if ($newController) {
                $this->cva = new ControllerViewArgs('\\Engine5\\Core\\Controller', $newController);
                $this->theme = 'default';
                $this->controller = $this->cva->getControllerInstance($this->templater);
                $this->controller->__preInit($this->cva);
            }
            $this->templater->setContainer($this->controller);

            $this->templater->setDryRun(true);
            $html = $this->templater->fetch($this->theme, $dir, $this);
        }

        return $html;
    }

    final protected function setResourceCompression($value) {
        Resource\Manager::getInstance('APP' . $this->name)->setResourceCompression($value);
    }

    final protected function getResourceCompression() {
        return Resource\Manager::getInstance('APP' . $this->name)->getResourceCompression();
    }

    final private function resource($type, $name, $priority, $plugin = null) {
        Resource\Manager::getInstance('APP' . $this->name)->add($type, $name, $priority, $plugin);
    }

    final protected function js($name, $priority = 5) {
        return $this->resource(Resource\Manager::RESOURCE_JS, $name, $priority);
    }

    final protected function app($name, $priority = 5) {
        return $this->resource(Resource\Manager::RESOURCE_APP, $name, $priority);
    }

    final protected function module($plugin, $name, $priority = 5) {
        return $this->resource(Resource\Manager::RESOURCE_MODULE, $name, $priority, $plugin);
    }

    final protected function css($name, $priority = 5) {
        return $this->resource(Resource\Manager::RESOURCE_CSS, $name, $priority);
    }

}
