<?php

namespace Engine5\Core;

/**
 * Description of Context
 *
 * @author barcis
 */
class Context {

    const CONTEXT_ENGINE = 'engine';
    const CONTEXT_APPLICATION = 'application';
    const CONTEXT_CONTROLLER = 'controller';
    const CONTEXT_REGION = 'region';
    const CONTEXT_VIEW = 'view';

    /**
     * @var array
     */
    private $context = array();

    /**
     * @var Context
     */
    private static $instance = null;

    public static function create() {
        if (!is_null(self::$instance)) {
            throw new Exception('redeclare context');
        }
        self::$instance = new Context();
    }

    /**
     *
     * @return Context
     * @throws Exception
     */
    public static function getInstamne() {
        if (is_null(self::$instance)) {
            throw new \Exception('brak instancji context');
        }

        return self::$instance;
    }

    private function __construct() {
        $this->context = array();
    }

    public static function setContext($context, $name) {

        array_unshift(self::$instance->context, new Context\Item($context, $name));
    }

    public static function unsetContext() {
        array_shift(self::$instance->context);
    }

    /**
     * @return Context\Item
     */
    public static function currentContext() {
        return reset(self::$instance->context);
    }

}
