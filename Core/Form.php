<?php

namespace Engine5\Core;

/**
 * Description of Form
 *
 * @author barciś
 */
final class Form implements \ArrayAccess {

    const POST = 'post';
    const GET = 'get';

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $region;

    /**
     * @var string
     */
    private $method;

    /**
     * @var Form\Fields
     */
    private $fields;

    /**
     * @var array
     */
    private $tabs;

    /**
     * @var array
     */
    private $errors;

    /**
     * @var array
     */
    private $infos;

    /**
     * @var bool
     */
    private $visible = true;

    /**
     * @var Form\Actions|Form\Action[]
     */
    private $actions = array();

    /**
     * @var array
     */
    private $actionsDone = array();

    /**
     * @var bool
     */
    private $hasFiles = array();

    /**
     * @var integer
     */
    private $maxFileSize = 20971520;
    private $isValid = 0;
    public $labelTags = false;

    public function setLabelTags($value) {
        $this->labelTags = $value;
    }

    public function getLabelTags() {
        return $this->labelTags;
    }

    /**
     * @param string $name
     * @param string $method
     * @throws Exception
     */
    public function __construct($name, $method, $region) {
        if ($method != self::GET && $method != self::POST) {
            throw new Exception('metoda dla formularza musi być podana jako \Engine5\Core\Form::POST lub \Engine5\Core\Form::GET');
        }

        $this->name = $name;
        $this->method = $method;
        $this->region = $region;
        $this->fields = new Form\Fields();
        $this->actions = new Form\Actions();
    }

    public function addError($msg) {
        $this->errors[] = new Form\Message($msg);
    }

    public function addInfo($msg) {
        $this->infos[] = new Form\Message($msg);
    }

    public function visible($isVisible = true) {
        $this->visible = $isVisible;
    }

    public function isVisible() {
        return (bool) $this->visible;
    }

    public function isValid() {
        if ($this->isValid === 0) {
            $this->isValid = 1;
            foreach ($this->fields as $field) {
                /* @var $field Form\Field */
                if (!$field->isValid()) {
                    $this->isValid = 2;
                }
            }
        }

        return ($this->isValid === 1);
    }

    public function hasErrors() {
        return !((bool) empty($this->errors));
    }

    public function hasInfos() {
        return (bool) !empty($this->infos);
    }

    public function hasFile() {
        return (bool) $this->hasFiles;
    }

    public function getMaxFileSize() {
        return $this->maxFileSize;
    }

    public function getErrors() {
        return $this->errors;
    }

    public function getInfos() {
        return $this->infos;
    }

    public function addTab($id, $name) {
        $this->tabs[$id] = $name;
    }

    public function addClone($from, $to, $defValue = null) {
        $this->fields[$to] = $this->fields[$from]->cloneTo($to);

        $_type = get_class($this->fields[$to]);
        $rfc = new ReflectionClass($_type);
        if ($rfc->implementsInterface('EI_file')) {
            $this->hasFiles = true;

            if ($defValue === null) {
                $value = Form\Manager::getInstance($this->region)->getRequestFileInfo($_type, $this->method, $to, $this->name);
                if ($value !== null) {
                    $defValue = $value;
                }
            }
        } else {
            $value = Form\Manager::getInstance($this->region)->getRequestValue($_type, $this->method, $to, $this->name);

            if ($value !== null) {
                $defValue = $value;
            }
        }

        $this->fields[$to]->setValue($defValue);

        return $this->fields[$to];
    }

    public function removeElement($name) {
        unset($this->fields[$name]);
    }

    public function addFields(array $fields) {
        foreach ($fields as $field) {
            $this->addField($field);
        }
    }

    public function addField(Form\Field $field) {
        $field->setForm($this);
        if (!($field instanceof \Engine5\Interfaces\Form\Field\Action)) {
            $value = Form\Manager::getInstance($this->region)->getRequestValue($field->getType(), $this->method, $field->getName(), $this->name);
            if ($value !== null) {
                $field->setValue($value);
            }
        } else {

        }

        $this->fields[$field->getName()] = $field;
        if ($field instanceof \Engine5\Interfaces\Form\Field\Action) {
            $this->actions[$field->getName()] = new Form\Action($field);
        }
    }

    /**
     *
     * @param string $name
     * @return Form\Action
     */
    public function getAction($name) {
        return $this->actions[$name];
    }

    public function postInit() {
        return;
        $fm = Form\Manager::getInstance($this->region);
        foreach ($this->fields as $field) {
            /* @var $field Form\Field */
            $_type = get_class($field);
            $value = $fm->getRequestValue($_type, $this->method, $field->getName(), $this->name);
            if ($value !== null) {
                $field->setValue($value);
            }
        }
    }

    public function getAttributes() {
        $attrs = array(
            'method' => $this->method,
            'name' => $this->name,
            'id' => $this->name,
        );

        if (!empty($this->action)) {
            $attrs['action'] = $this->action;
        }
        if ($this->hasFiles) {
            $attrs['enctype'] = "multipart/form-data";
        }
        return $attrs;
    }

    public function getName() {
        return $this->name;
    }

    /**
     *
     * @return Form\Fields
     */
    public function getFields() {
        return $this->fields;
    }

    public function getTabs() {
        return $this->tabs;
    }

    /**
     * @param string $name
     * @return Form\Field
     */
    public function getField($name) {
        return $this->fields[$name];
    }

    public function offsetExists($name) {
        return isset($this->fields[$name]);
    }

    public function offsetGet($name) {
        if (isset($this->fields[$name])) {
            return $this->fields[$name]->getValue();
        }
    }

    public function offsetSet($name, $value) {
        if (isset($this->fields[$name])) {
            $this->fields[$name]->setValue($value);
        }
    }

    public function offsetUnset($offset) {
        throw new Exception('Nie zaimplementowane!');
    }

    public function __get($name) {
        return $this->offsetGet($name);
    }

    public function __set($name, $value) {
        $this->offsetSet($name, $value);
        $this->offsetSet($name, $value);
    }

    public function __isset($name) {
        return $this->offsetExists($name);
    }

    public function asArray() {
        $tmp = array();

        foreach ($this->fields as $field) {
            $tmp[$field->getName()] = $field->getValue();
        }
        return $tmp;
    }

    public function readFromModel(E_model $model, array $model2field = null) {
        if ($model2field === null) {
            foreach ($model->asArray() as $name => $value) {
                if (isset($this->fields[$name])) {
                    $this->fields[$name]->setValue($value);
                }
            }
        } else {
            foreach ($model->asArray() as $name => $value) {
                if (isset($model2field[$name]) && isset($this->fields[$model2field[$name]])) {
                    $this->fields[$name]->setValue($value);
                }
            }
        }
    }

    public function writeToModel(E_model &$model, array $field2model = array()) {
        foreach ($field2model as $mname => $fname) {
            $model->{$mname} = $this->fields[$fname]->getValueForSql();
        }
    }

}
