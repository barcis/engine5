<?php

namespace Engine5\Core;

use Engine5\Core\Engine\Entrypoint;

class Debug {

    private static $firstUseP = true;
    private static $times = [];

    public static function p($data, $forceFull = false) {
        if (!$forceFull && (Engine::getEntryPoint() === Entrypoint::SCRIPT || Engine::getEntryPoint() === Entrypoint::REST)) {
            if (isset($data)) {
                echo print_r($data, true);
            } else {
                echo 'null';
            }
            echo "\n";
            return;
        }

        if (Engine::getEntryPoint() === Entrypoint::REST) {
            for ($a = 0; $a < count($f); $a++) {
                if (isset($f[$a]['file']) && isset($f[$a]['line'])) {
                    if (Engine::getEntryPoint() !== Entrypoint::AJAX) {
                        print_r((isset($f[$a]['function']) ? '*' . $f[$a]['function'] . '()* in ' : '') . $f[$a]['file'] . ':' . $f[$a]['line'] . "\n");
                    } else {
                        print_r((isset($f[$a]['function']) ? $f[$a]['function'] . '() in ' : '') . $f[$a]['file'] . ':' . $f[$a]['line'] . "\n");
                    }
                }
            }
        } elseif (self::$firstUseP &&
                Engine::getEntryPoint() !== Entrypoint::AJAX &&
                Engine::getEntryPoint() !== Entrypoint::SCRIPT &&
                Engine::getEntryPoint() !== Entrypoint::REST) {
            echo "
        <style>
            pre.debug { color:black; text-align:left; background-color:yellow; padding:5px; border:2px solid black; }
            pre.debug span.files { text-align:left; background-color: rgba(255, 184, 0, 0.34);; color:#000; border:1px solid black; display:none; padding:1px 5px; position:absolute; left:50%;}
            pre.debug:hover span.files { display:block; }
        </style>";
        }
        self::$firstUseP = false;

        $linesLimit = 99;

        $bt = debug_backtrace();
        $f = array();

        for ($a = 0; $a < $linesLimit; $a++) {
            $f[$a] = array_shift($bt);
            if ($f[$a]['function'] == 'pd') {
                $f[$a] = array_shift($bt);
            }
        }

        if (Engine::getEntryPoint() !== Entrypoint::AJAX) {
            echo '<pre class="debug">';
            echo '<span class="files">';
        }

        for ($a = 0; $a < count($f); $a++) {
            if (isset($f[$a]['file']) && isset($f[$a]['line'])) {
                if (Engine::getEntryPoint() !== Entrypoint::AJAX) {
                    print_r((isset($f[$a]['function']) ? '<b>' . $f[$a]['function'] . '()</b> in ' : '') . $f[$a]['file'] . ':' . $f[$a]['line'] . "\n");
                } else {
                    print_r((isset($f[$a]['function']) ? $f[$a]['function'] . '() in ' : '') . $f[$a]['file'] . ':' . $f[$a]['line'] . "\n");
                }
            }
        }

        if (Engine::getEntryPoint() !== Entrypoint::AJAX) {
            echo '</span>';
        }
        unset($bt, $f);

        if (isset($data)) {
            if (Engine::getEntryPoint() !== Entrypoint::AJAX) {
                echo htmlentities(print_r($data, true));
            } else {
                echo print_r($data, true);
            }
        } else {
            echo 'null';
        }
        if (Engine::getEntryPoint() !== Entrypoint::AJAX) {
            echo '</pre>';
        }
    }

    public static function pd($data, $forceFull = false) {
        http_response_code(409);
        self::p($data, $forceFull);
        die();
    }

    public static function beginTimeLogSession($name = 'default') {
        self::$times[$name] = new DebugExecutionTime($name);
    }

    /**
     *
     * @param string $name
     * @return DebugExecutionTime
     */
    public static function endTimeLogSession($name = 'default') {
        self::$times[$name]->stamp();
        return self::$times[$name];
    }

    /**
     *
     * @param string $name
     * @return DebugExecutionTime
     */
    public static function getTimeLogSession($name = 'default') {
        return self::$times[$name];
    }

}
