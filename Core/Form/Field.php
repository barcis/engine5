<?php

namespace Engine5\Core\Form;

/**
 * Description of Field
 *
 * @author barcis
 */
abstract class Field {

    private $label;
    private $placeholder;
    private $name;
    private $value;
    private $type;
    private $tag;
    private $isShort;
    private $attribs = array();
    private $modifiers = array();
    private $validators = array();
    private $e5js = array();
    private $required = false;
    private $disabled = false;
    private $hasError = false;

    /**
     * @var array
     */
    private $additionals = array();

    /**
     * @var array
     */
    private $errors = array();

    /**
     * \Engine5\Core\Form $form
     */
    private $form = null;

    public function cloneTo($to) {
        $a = clone $this;
        $a->name = $to;
        return $a;
    }

    public function additional($name, $value = null) {
        //getter
        if (func_num_args() == 1) {
            if (isset($this->additionals[$name])) {
                return $this->additionals[$name];
            }
            return null;
        } elseif (func_num_args() == 2) {
            $this->additionals[$name] = $value;
        }
        return $this;
    }

    public function addModifier($modifier) {
        if (!isset($this->modifiers[$modifier])) {
            $this->execModifier($modifier);
            $this->modifiers[$modifier] = $modifier;
        }
        return $this;
    }

    public function removeModifier($modifier) {
        if (isset($this->modifiers[$modifier])) {
            unset($this->modifiers[$modifier]);
        }
        return $this;
    }

    public function getValidators() {
        return array_keys($this->validators);
    }

    public function addValidator($validator, $params = array()) {
        if (!isset($this->validator[$validator])) {
            $rfc = new \ReflectionClass('V_' . $validator);
            $this->validators[$validator] = $rfc->newInstance($this, $params);
        }
        return $this;
    }

    public function removeValidator($validator) {
        if (isset($this->validators[$validator])) {
            unset($this->validators[$validator]);
        }
        return $this;
    }

    public function addE5JsAction($trigger, $action) {
        $this->e5js['trigger'] = $trigger;
        $this->e5js['action'] = $action;
        return $this;
    }

    public function addE5JsAttribute($name, $value) {
        if (!isset($this->e5js[$name])) {
            $this->e5js[$name] = $value;
        }
        return $this;
    }

    public function removeE5JsAttribute($name) {
        if (isset($this->e5js[$name])) {
            unset($this->e5js[$name]);
        }
        return $this;
    }

    public function required($value = true) {
        $this->required = $value;
        return $this;
    }

    public function disabled($value) {
        $this->disabled = $value;
        return $this;
    }

    public function isDisabled() {
        return $this->disabled;
    }

    public function getName() {
        return $this->name;
    }

    public function getLabel() {
        return $this->label;
    }

    public function getValue() {
        return $this->value;
    }

    public function getValueForSql() {
        if (!is_array($this->value)) {
            return stripslashes($this->value);
        } else {
            return $this->value;
        }
    }

    public function getType() {
        return $this->type;
    }

    public function getTag() {
        return $this->tag;
    }

    public function isShort() {
        return $this->isShort;
    }

    public function isRequired() {
        return $this->required;
    }

    public function setValue($value) {
        $this->value = $value;
        if (!empty($this->modifiers)) {
            $this->execModifiers();
        }
    }

    protected function setTag($value) {
        $this->tag = $value;
    }

    protected function setShort($value) {
        $this->isShort = $value;
    }

    protected function setDefaultAttribs(array $value) {
        $this->attribs = $value;
    }

    protected function addAttr($name, $value) {
        $this->attribs[$name] = $value;
    }

    protected function removeAttr($name) {
        unset($this->attribs[$name]);
    }

    protected function getNameAttr($region, $form) {
        return $this->type . '[' . implode('][', array($region, $form, $this->name)) . ']' . (isset($this->attribs['multiple']) && $this->attribs['multiple'] == 'multiple' ? '[]' : '');
    }

    public function getE5JsAttributes() {
        return $this->e5js;
    }

    public function getAttributes($region, $form) {
        $attrs = array_merge(array('id' => $form . '-' . $this->name, 'name' => $this->getNameAttr($region, $form)), $this->attribs);
        if ($this->isShort) {
            return array_merge($attrs, $this->renderValue());
        } else {
            return $attrs;
        }
    }

    public function __construct($name, $value = null) {
        $this->name = $name;
        $this->value = $value;
        $this->type = get_class($this);
    }

    public function setForm($form) {
        $this->form = $form;
        return $this;
    }

    public function setLabel($label) {
        $this->label = $label;
        return $this;
    }

    public function setPlaceholder($placeholder) {
        $this->placeholder = $placeholder;
        return $this;
    }

    public function getPlaceholder() {
        return $this->placeholder;
    }

    private function execModifiers() {
        foreach ($this->modifiers as $modifier) {
            $rfc = new ReflectionClass('E_modifier_' . $modifier);
            $this->value = call_user_func(array($rfc->newInstance(), 'exec'), $this->value);
        }
    }

    private function execModifier($modifier) {
        $rfc = new ReflectionClass('E_modifier_' . $modifier);
        $this->value = call_user_func(array($rfc->newInstance(), 'exec'), $this->value);
    }

    public function renderValue() {
        if ($this->isShort) {
            return array('value' => $this->value);
        } else {
            return $this->value;
        }
    }

    public function addError($msg) {
        $this->errors[] = new Message($msg);
    }

    public function hasErrors() {
        return $this->hasError;
    }

    public function getErrors() {
        return $this->errors;
    }

    public function isValid() {
        if ($this->required && (($this instanceof Field\Checkbox && $this->value === 'off') || empty($this->value))) {
            $message = $this->getLabel() . ' to pole wymagane';
            $this->addError($message);
            $this->form->addError($message);
            $this->hasError = true;
            return false;
        } else {
            $isValid = true;
            foreach ($this->validators as $validator) {
                /* @var $validator E_validator */
                $messages = $validator->validate();
                if ($messages !== true) {
                    if (is_array($messages)) {
                        foreach ($messages as $message) {
                            $this->addError($message);
                            $this->form->addError($message);
                        }
                    }
                    $isValid = false;
                }
            }

            $this->hasError = !$isValid;

            return $isValid;
        }
    }

    public function asArray() {
        $arr = [];
        foreach ($this as $key => $value) {
            if (in_array($key, ['form'])) {
                continue;
            }
            $arr[$key] = $value;
        }
        return $arr;
    }

}
