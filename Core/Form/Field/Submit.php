<?php

namespace Engine5\Core\Form\Field;

/**
 * Description of Submit
 *
 * @author barcis
 */
class Submit extends \Engine5\Core\Form\Field implements \Engine5\Interfaces\Form\Field\Action {

    private $action;

    public function __construct($name, $value = null) {
        parent::__construct($name, $value);
        parent::setShort(false);
        parent::setTag('button');
        parent::setDefaultAttribs(array('type' => 'submit'));
    }

    public function setAction(callable $action) {
        $this->action = $action;
        return $this;
    }

    /**
     * @return callable
     */
    public function getAction() {
        return $this->action;
    }

}
