<?php

namespace Engine5\Core\Form\Field;

/**
 * Description of Textarea
 *
 * @author barcis
 */
class Textarea extends \Engine5\Core\Form\Field {

    public function __construct($name, $value = null) {
        parent::__construct($name, $value);
        parent::setShort(false);
        parent::setTag('textarea');
    }

}
