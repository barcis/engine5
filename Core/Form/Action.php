<?php

namespace Engine5\Core\Form;

/**
 * Description of Action
 *
 * @author barcis
 */
class Action {

    /**
     *
     * @var \Engine5\Interfaces\Form\Field\Action
     */
    private $field;
    private $done = false;

    public function __construct(\Engine5\Interfaces\Form\Field\Action $field) {
        $this->field = $field;
    }

    /**
     *
     * @return Action
     */
    public function getAction() {
        return $this->field->getAction();
    }

    public function isDone() {
        return $this->done;
    }

    public function setDone() {
        $this->done = true;
    }

}
