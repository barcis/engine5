<?php

namespace Engine5\Core;

/**
 * Description of Controller
 *
 * Domyślny kontroler w systemie.
 * Obsługuje wszystkie niezadeklarowane klasy kontrolerów.
 * Regiony wybiera z bazy danych z tabeli regions
 *
 * @author barcis
 */
class Controller implements \Engine5\Interfaces\Controller {

    private $params = array();
    private $cache = false;
    private $regions;

    /**
     * @var Placeholders
     */
    private $placeholders;

    /**
     * @var ITemplater
     */
    protected $templater;

    public final function __construct(\Engine5\Interfaces\Templater $templater, array $params = array(), $cache = false) {
        $this->params = $params;
        $this->cache = $cache;
        $this->placeholders = new Placeholders();
        $this->templater = &$templater;

        $this->placeholders['__INLINE_PLACEHOLDER__'] = new Placeholder('__INLINE_PLACEHOLDER__', $this->templater);
    }

    public function __init() {

    }

    public function __get($name) {
        if ($name == 'params') {
            return $this->params;
        }
    }

    public final function __preInit(\Engine5\Core\ControllerViewArgs &$controllerViewArgs) {
        Context::setContext(Context::CONTEXT_REGION, 'R_' . $controllerViewArgs->action);
        $rfc = new \ReflectionClass($controllerViewArgs->region);

        $this->regions['R_main'] = $rfc->newInstance($controllerViewArgs->region, $controllerViewArgs->view, $this->templater, $this->params, $controllerViewArgs);
        Context::unsetContext();
        if (!isset($this->placeholders['default'])) {
            $this->placeholders['default'] = new Placeholder('default', $this->templater);
        }
        @$this->placeholders['default']['R_main'] = $this->regions['R_main'];
    }

    private $errors = [];

    public final function getCacheEnabled() {
        return !($this->cache === false) && ($this->cache['enabled'] === true);
    }

    public final function getCacheIncludeLogin() {
        return (isset($this->cache['include-login']) && $this->cache['include-login'] === true);
    }

    public final function getCacheTimeout() {
        return isset($this->cache['timeout']) ? (int) $this->cache['timeout'] : 3600;
    }

    public final function hasErrors() {
        return count($this->errors);
    }

    public final function getErrors() {
        return $this->errors;
    }

    public final function addError($error) {
        $this->errors[] = [
            'context' => Context::currentContext(),
            'error' => $error
        ];
    }

    /**
     *
     * @param type $name
     * @return Placeholder
     */
    public function getPlaceHolder($name) {
        if (isset($this->placeholders[$name])) {
            return $this->placeholders[$name];
        }
        return NULL;
    }

}
