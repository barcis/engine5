<?php

namespace Engine5\Helper;

trait Angular {

    private $files = ['app' => 20, 'configs' => 21, 'helpers' => 25, 'controllers' => 30, 'router' => 35];
    private $dirs = ['class', 'controllers', 'directives', 'langs', 'factories', 'services', 'filters', 'models', 'values'];

    private function loadAngularModuleParts($module, $path) {
        $rc = new \ReflectionClass(__CLASS__);

        $fullPath = dirname($rc->getFileName()) . '/frontend/app/' . $path . '/';
        if (file_exists($fullPath)) {
            $dir = dir($fullPath);

            while ($filename = $dir->read()) {
                $file = $fullPath . $filename;
                if (is_file($file) && preg_match('/\.js$/', $filename)) {
                    $fn = preg_replace('/\.js$/', '', $filename);
                    $this->module($module, $path . '/' . $fn, 40);
                }
            }
        }
    }

    private function loadAngularAppParts($path) {
        $fullPath = ST_APPDIR . '/frontend/app/' . $path . '/';
        if (file_exists($fullPath)) {
            $dir = dir($fullPath);

            while ($filename = $dir->read()) {
                $file = $fullPath . $filename;
                if (is_file($file) && preg_match('/\.js$/', $filename)) {
                    $fn = preg_replace('/\.js$/', '', $filename);
                    $this->app($path . '/' . $fn, 40);
                }
            }
        }
    }

    public function loadAngularModule($module) {
        foreach ($this->files as $file => $priority) {
            $this->module($module, $file, $priority);
        }

        foreach ($this->dirs as $dir) {
            $this->loadAngularModuleParts($module, $dir);
        }
    }

    public function loadAngularApp() {
        foreach ($this->files as $file => $priority) {
            if (file_exists(ST_APPDIR . '/frontend/app/' . $file . '.js')) {
                $this->app($file, $priority);
            }
        }

        foreach ($this->dirs as $dir) {
            $this->loadAngularAppParts($dir);
        }

        $AppName = \Engine5\Core\Engine::getCurrentAppName();
        $AppConfig = \Engine5\Core\Engine::getConfig()->configs[$AppName];


        if (isset($AppConfig['plugins'])) {
            foreach ($AppConfig['plugins'] as $plugin) {
                $rc = new \ReflectionClass($plugin['class']);

                if ($rc->hasMethod('scripts')) {
                    $scripts = $rc->getMethod('scripts')->invoke(null);
                    if (is_array($scripts)) {
                        foreach ($scripts as $script) {
                            $this->js($script, 40);
                        }
                    }
                }
                if ($rc->hasMethod('styles')) {
                    $styles = $rc->getMethod('styles')->invoke(null);
                    if (is_array($styles)) {
                        foreach ($styles as $style) {
                            $this->css($style, 40);
                        }
                    }
                }
            }
        }
    }

}
