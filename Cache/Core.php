<?php

namespace Engine5\Cache;

/**
 * Description of Core
 *
 * @author barcis
 */
class Core {

    private $enabled = false;
    private $types = [];

    /**
     *
     * @var self
     */
    private static $instance;

    /**
     *
     * @return self
     */
    public static function instance() {
        if (self::$instance == null) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    private function __construct(/* $basedir, array $types = [] */) {
        $configFile = ST_CONFIGDIR . '/cache.config.yml';
        if (file_exists($configFile)) {
            $cacheConfigs = \Engine5\Tools\Yaml::parseFile($configFile);

            $config = new Config($cacheConfigs);

            if (isset($config->enabled) && $config->enabled === true) {
                $this->enabled = true;
                $this->registerCacheTypes($config->types);
            }
        }
    }

    public function registerCacheTypes(array $types) {
        foreach ($types as $name => $config) {
            $this->registerCacheType($name, $config);
        }
    }

    public function registerCacheType($name, $config) {
        $storageClass = $config['storage'];
        unset($config['storage']);
        $rf = new \ReflectionClass($storageClass);

        $this->types[$name] = $rf->newInstance($name, $config);
    }

    public static function get($name, $id, $timeout = null) {
        if (!isset(self::instance()->types[$name])) {
            throw new \Engine5\Cache\CacheExpireException('disabled', 0);
        }
        return self::instance()->types[$name]->get($id, $timeout);
    }

    public static function getConfig($name, $id, $timeout = null) {
        if (!isset(self::instance()->types[$name])) {
            throw new \Engine5\Cache\CacheExpireException('disabled', 0);
        }
        return self::instance()->types[$name]->getConfig($id, $timeout);
    }

    public static function set($name, $id, $data, $timeout = null) {
        if (!isset(self::instance()->types[$name])) {
            return;
        }
        return self::instance()->types[$name]->set($id, $data, $timeout);
    }

}
