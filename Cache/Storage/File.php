<?php

namespace Engine5\Cache\Storage;

/**
 * Description of File
 *
 * @author barcis
 */
class File implements \Engine5\Cache\Storage {

    private $name;
    private $enabled;
    private $dir;
    private $timeout;
    private $configs = [];

    public function __construct($name, $config) {
        $this->name = $name;
        $this->enabled = isset($config['enabled']) && ($config['enabled'] === true);

        $this->dir = $config['dir'];
        $this->timeout = (int) $config['timeout'];

        if (!file_exists($this->getDirName()) || !is_dir($this->getDirName())) {
            mkdir($this->getDirName(), 0777);
        }
    }

    private function getDirName() {
        return ST_VAR . DIRECTORY_SEPARATOR . $this->dir . DIRECTORY_SEPARATOR;
    }

    private function getFileName($id) {
        return $this->getDirName() . md5($id) . '.cache';
    }

    public function isEnabled() {
        return $this->enabled;
    }

    public function get($id, $timeout = null) {

        $timeout = ($timeout) ? $timeout : $this->timeout;

        $filename = $this->getFileName($id);
        if ($this->isEnabled() && file_exists($filename) && (time() - filectime($filename) < $timeout)) {
            $ctime = filectime($filename);
            $this->configs[$filename] = [
                'cached' => true,
                'timeout' => $timeout,
                'created' => $ctime,
                'expires' => $ctime + $timeout,
            ];

            return unserialize(file_get_contents($filename));
        } else {
            throw new \Engine5\Cache\CacheExpireException('Cache ' . $filename . ' expired');
        }
    }

    public function getConfig($id, $timeout = null) {

        $timeout = ($timeout) ? $timeout : $timeout;

        $filename = $this->getFileName($id);

        if (isset($this->configs[$filename])) {
            return $this->configs[$filename];
        }

        if (file_exists($filename) && (time() - filectime($filename) < $timeout)) {
            $ctime = filectime($filename);
            $this->configs[$filename] = [
                'cached' => true,
                'timeout' => $timeout,
                'created' => $ctime,
                'expires' => $ctime + $timeout,
            ];
            return $this->configs[$filename];
        }

        return [
            'cached' => false,
            'timeout' => 0,
            'created' => 0,
            'expires' => 0,
        ];
    }

    public function set($id, $data, $timeout = null) {
        $filename = $this->getFileName($id);
        file_put_contents($filename, serialize($data));
    }

}
