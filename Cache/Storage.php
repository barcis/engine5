<?php

namespace Engine5\Cache;

/**
 * Description of Storage
 *
 * @author barcis
 */
interface Storage {

    public function isEnabled();

    public function get($id, $timeout = null);

    public function getConfig($id, $timeout = null);

    public function set($id, $data, $timeout = null);
}
