<?php

namespace Engine5\Factory;

use \Engine5\Core\Engine\App\Config\Templater as Config;
use \Engine5\Core\Engine\Config\Parser\Yaml;
use \Engine5\Core\Engine;

/**
 * Description of Templater
 *
 * @author barcis
 */
class Templater {

    /**
     * @return Templater
     */
    public static function newInstance() {
        $config = Engine::getConfig()->currentAppConfig['templater'];
        $yConfig = new Yaml($config['file']);
        return self::newSpecyficInstance($yConfig->type, new Config($yConfig));
    }

    /**
     * @return Templater
     */
    public static function newSpecyficInstance($type, Config $config) {
        $rfc = new \ReflectionClass($type);

        $templater = $rfc->newInstance();
        /* @var $templater Templater */

        $templater->init($config);
        return $templater;
    }

}
