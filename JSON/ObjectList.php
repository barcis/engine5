<?php

namespace Engine5\JSON;

/**
 * Description of ObjectList
 *
 * @author barcis
 */
class ObjectList implements \ArrayAccess, \Iterator, \JsonSerializable, \Countable {

    /**
     *
     * @var int
     */
    private $position = 0;

    /**
     * @var array
     */
    private $_data;

    public function __construct(array $data) {
        $this->setData($data);
        $this->position = 0;
    }

    final protected function setData(array $data) {
        $this->_data = $data;
    }

    final public function findIndex($field, $needed) {
        foreach ($this->_data as $idx => $row) {
            if ($row->{$field} === $needed) {
                return $idx;
            }
        }
    }

    final public function find($field, $needed) {
        foreach ($this->_data as $row) {
            if (is_object($row) && $row->{$field} === $needed) {
                return $row;
            } elseif (is_array($row) && $row[$field] === $needed) {
                return $row;
            }
        }
    }

    public function offsetExists($offset) {
        return isset($this->_data[$offset]);
    }

    public function offsetGet($offset) {
        return $this->_data[$offset];
    }

    public function offsetSet($offset, $value) {
        if (is_null($offset)) {
            $this->_data[] = $value;
        } else {
            $this->_data[$offset] = $value;
        }
    }

    public function offsetUnset($offset) {
        unset($this->_data[$offset]);
    }

    public function jsonSerialize() {
        return array_values($this->_data);
    }

    public function count() {
        return count($this->_data);
    }

    public function isEmpty() {
        return ($this->count() === 0);
    }

    public function rewind() {
        $this->position = 0;
    }

    public function current() {
        $tmp = array_keys($this->_data);
        if (isset($tmp[$this->position]) && isset($this->_data[$tmp[$this->position]])) {
            return $this->_data[$tmp[$this->position]];
        }
        return null;
    }

    public function key() {
        return $this->position;
    }

    public function next() {
        $this->position++;
    }

    public function valid() {
        $tmp = array_keys($this->_data);
        return isset($tmp[$this->position]);
    }

    public function getItems() {
        return $this->_data;
    }

    public function first() {
        return reset($this->_data);
    }

    public function last() {
        return end($this->_data);
    }

}
