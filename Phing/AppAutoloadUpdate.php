<?php

require_once "phing/Task.php";

class AppAutoloadUpdate extends Task {

    private $file = null;
    private $brand = null;

    public function setFile($str) {
        $this->file = $str;
    }

    public function setBrand($str) {
        $this->brand = $str;
    }

    public function init() {

    }

    public function main() {
        $config = json_decode(file_get_contents($this->file), true);
        if (!isset($config['autoload'])) {
            $config['autoload'] = [];
        }

        if (!isset($config['autoload']['psr-4'])) {
            $config['autoload']['psr-4'] = [];
        }
        if (!isset($config['autoload']['files'])) {
            $config['autoload']['files'] = [];
        }

        if (array_search('vendor/batusystems/engine5/Core/Engine.php', $config['autoload']['files']) === false) {
            $config['autoload']['files'][] = 'vendor/batusystems/engine5/Core/Engine.php';
        }
        $psr4 = [
            $this->brand . '\\' => 'apps/content/',
            $this->brand . '\\Model\\' => 'apps/model/'
        ];


        foreach ($psr4 as $key => $value) {
            if (!isset($config['autoload']['psr-4'][$key])) {
                $config['autoload']['psr-4'][$key] = $value;
            }
        }

        file_put_contents($this->file, json_encode($config, JSON_PRETTY_PRINT));
    }

}
