<?php

require_once "phing/Task.php";

class AppsConfigUpdate extends Task {

    private $file = null;
    private $name = null;
    private $domain = null;
    private $brand = null;

    public function setFile($str) {
        $this->file = $str;
    }

    public function setName($str) {
        $this->name = $str;
    }

    public function setDomain($str) {
        $this->domain = $str;
    }

    public function setBrand($str) {
        $this->brand = $str;
    }

    public function init() {

    }

    public function main() {
        $yaml = \Engine5\Tools\Yaml::parseFile($this->file);

        if (!isset($yaml['applications'][$this->domain])) {
            $yaml['applications'][$this->domain] = ['/' => $this->name];
        }
        if (!isset($yaml['configs'][$this->name])) {
            $yaml['configs'][$this->name] = [
                'defaultDomain' => 'http://' . $this->domain . '/',
                'invoker' => '\\' . $this->brand . '\\' . $this->name . '\\App',
                'database' => [
                    'type' => 'yaml',
                    'file' => 'database.config.yml'
                ],
                'templater' => [
                    'type' => 'smarty',
                    'file' => 'tamplater.config.yml'
                ]
            ];
        }

        \Engine5\Tools\Yaml::saveFile($this->file, (array) $yaml);
    }

}
