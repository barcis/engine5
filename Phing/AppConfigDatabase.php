<?php

require_once "phing/Task.php";

class AppConfigDatabase extends Task {

    private $brand = null;
    private $file = null;
    private $type = null;
    private $host = null;
    private $name = null;
    private $user = null;
    private $pass = null;
    private $port = 5432;
    private $create = false;
    private $env = null;

    public function setEnv($env) {
        $this->env = $env;
    }

    public function setPort($port) {
        $this->port = $port;
    }

    public function setCreate($create) {
        echo "create '{$create}'\n\n";
        $this->create = ($create == 't' || $create == 1);
    }

    public function setBrand($str) {
        $this->brand = $str;
    }

    public function setFile($file) {
        $this->file = $file;
    }

    public function setType($type) {
        $this->type = $type;
    }

    public function setHost($host) {
        $this->host = $host;
    }

    public function setName($name) {
        $this->name = $name;
    }

    public function setUser($user) {
        $this->user = $user;
    }

    public function setPass($pass) {
        $this->pass = $pass;
    }

    public function init() {

    }

    public function main() {
        if (file_exists($this->file)) {
            $yaml = \Engine5\Tools\Yaml::parseFile($this->file);
        } else {
            $yaml = [];
        }

        if (!isset($yaml['default'])) {
            $yaml['default'] = 'devel';
        }
        if (!isset($yaml['databases'])) {
            $yaml['databases'] = [];
        }
        $yaml['databases'][$this->env] = [
            'type' => $this->type,
            'host' => $this->host,
            'name' => $this->name,
            'user' => $this->user,
            'pass' => $this->pass,
            'port' => $this->port,
            'prefix' => '',
            'models' => [
                '\\' . $this->brand . '\\Model'
            ]
        ];

        \Engine5\Tools\Yaml::saveFile($this->file, (array) $yaml);

        $conn = @pg_connect("host={$this->host} port={$this->port} dbname={$this->name} user={$this->user} password={$this->pass}");
        if (!$conn) {
            echo("\n\nconnection faild!!!\n\n");
            if ($this->create) {
                $conn = @pg_connect("host={$this->host} port={$this->port} dbname=postgres user={$this->user} password={$this->pass}");
                if ($conn) {
                    $result = pg_query($conn, "CREATE DATABASE {$this->name};");
                    if (!$result) {
                        echo "An error occurred.\n";
                        var_export($result);
                    } else {
                        echo("\n\ndatabase created!!!\n\nCREATE DATABASE {$this->name};\n\n");
                    }
                } else {
                    echo("\n\nconnection master database faild!!!\n\n");
                }
            }
        } else {
            echo("\n\nconnection ok!!!\n\n");
        }
    }

}
