<?php

namespace Engine5\Tools;

/**
 * Description of Object
 *
 * @author barcis
 */
class Object {

    /**
     *
     * @param object $obj
     * @param string $_path
     * @return mixed
     */
    public static function retrieve(&$obj, $_path) {
        $path = explode('.', $_path);
        $element = $obj;
        foreach ($path as $step) {
            if (is_array($element) && isset($element[$step])) {
                $element = $element[$step];
            } elseif (is_object($element) && isset($element->{$step})) {
                $element = $element->{$step};
            } else {
                $element = null;
                break;
            }
        }
        return $element;
    }

}
