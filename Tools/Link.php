<?php

namespace Engine5\Tools;

/**
 * Description of Link
 *
 * @author barcis
 */
class Link {

    static public function url($target, $params = array(), $type = 'Standard', $withBase = true, $app = '') {
        $cva = new \Engine5\Core\ControllerViewArgs('$tokens[0]', $target, $params, '', $app);
        return \Engine5\Factory\Router::newInstance([], $type)->translateToUrl($cva, $withBase);
    }

    static public function resolve($url, $app = null, $type = 'Standard') {
        $router = \Engine5\Factory\Router::newInstance([], $type);
        return $router->translateFromUrl($url, null, $app);
    }

}
