<?php

namespace Engine5\Tools;

/**
 * Description of DataObject
 *
 * @author barcis
 */
class DataObject {

    public function __construct($input) {
        $rfc = new \ReflectionClass(get_class($this));

        foreach ($rfc->getProperties() as $property) {
            /* @var $property ReflectionProperty */
            $name = $property->getName();

            if (is_array($input) && isset($input[$name])) {
                $value = $input[$name];
            } elseif (is_object($input) && isset($input->{$name})) {
                $value = $input->{$name};
            } else {
                continue;
            }

            $fname = 'load' . ucfirst($name);
            if (method_exists($this, $fname)) {
                $this->{$fname}($value);
            } else {
                $this->{$name} = $value;
            }
        }
    }

}
