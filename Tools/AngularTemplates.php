<?php

namespace Engine5\Tools;

/**
 * Description of AngularTemplates
 *
 * @author barcis
 */
class AngularTemplates {

    private static function concat(&$value, $params) {
        if (!isset($params[1])) {
            $params[1] = ',';
        }

        $retrun = '';
        if (is_array($value)) {
            if (isset($params[2])) {
                foreach ($value as $v) {
                    if (isset($v->{$params[2]})) {
                        $retrun .= $params[1] . $v->{$params[2]};
                    }
                }
            } else {
                $retrun = implode($params[1], $value);
            }
        }

        $value = trim($retrun, $params[1]);
    }

    public static function resolve($template, &$additionalParams) {
        if (count($additionalParams) > 0) {
            preg_match_all('/{{(?<variable>[a-z0-9\_\.]+)(\|(?<filter>[^\|{}]+))?}}/i', $template, $_variables);
            if (isset($_variables['variable']) && is_array($_variables['variable']) && count($_variables['variable'] > 0)) {

                $variables = $_variables['variable'];
                $values = [];
                foreach ($variables as $idx => $variable) {
                    $value = \Engine5\Tools\Object::retrieve($additionalParams, $variable);
                    if (isset($_variables['filter'][$idx]) && strlen($_variables['filter'][$idx]) > 0) {
                        $params = explode(':', $_variables['filter'][$idx]);
                        $filter = $params[0];
                        unset($params[0]);
                        self::{$filter}($value, $params);
                    }
                    $values[$_variables[0][$idx]] = $value;
                }

                $template = str_replace(array_keys($values), array_values($values), $template);
            }
        }
        return $template;
    }

}
