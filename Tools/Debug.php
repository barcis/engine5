<?php

function p() {
    $info = debug_backtrace(DEBUG_BACKTRACE_PROVIDE_OBJECT, 1)[0];
    $lines = file($info['file']);
    $invokeLine = trim($lines[$info['line'] - 1]);
    preg_match('/pd?\((.+)\)/', $invokeLine, $matches);

    $args = [];
    if (isset($matches[1])) {
        $args = explode(',', str_replace(' ', '', $matches[1]));
    }
    $arguments = array_combine($args, func_get_args());
    $arguments['__invoke'] = $invokeLine;

    \Engine5\Core\Debug::p(func_get_args(), false);
}

function pf() {
    $info = debug_backtrace(DEBUG_BACKTRACE_PROVIDE_OBJECT, 1)[0];
    $lines = file($info['file']);
    $invokeLine = trim($lines[$info['line'] - 1]);
    preg_match('/pd?\((.+)\)/', $invokeLine, $matches);

    $args = [];
    if (isset($matches[1])) {
        $args = explode(',', str_replace(' ', '', $matches[1]));
    }
    $arguments = array_combine($args, func_get_args());
    $arguments['__invoke'] = $invokeLine;

    \Engine5\Core\Debug::p(func_get_args(), true);
}

function pdf() {
    $info = debug_backtrace(DEBUG_BACKTRACE_PROVIDE_OBJECT, 1)[0];
    $lines = file($info['file']);
    $invokeLine = trim($lines[$info['line'] - 1]);
    preg_match('/pd?\((.+)\)/', $invokeLine, $matches);

    $args = [];
    if (isset($matches[1])) {
        $args = explode(',', str_replace(' ', '', $matches[1]));
    }
    $arguments = array_combine($args, func_get_args());
    $arguments['__invoke'] = $invokeLine;

    \Engine5\Core\Debug::pd(func_get_args(), true);
}

function pc($value) {
    \PhpConsole\Handler::getInstance()->debug($value);
}

function pd() {
    $info = debug_backtrace(DEBUG_BACKTRACE_PROVIDE_OBJECT, 1)[0];
    $lines = file($info['file']);
    $invokeLine = trim($lines[$info['line'] - 1]);
    preg_match('/pd?\((.+)\)/', $invokeLine, $matches);

    $args = [];
    if (isset($matches[1])) {
        $args = explode(',', str_replace(' ', '', $matches[1]));
    }
    $arguments = array_combine($args, func_get_args());
    $arguments['__invoke'] = $invokeLine;

    \Engine5\Core\Debug::pd($arguments, false);
}

function dd() {
    $datas = func_get_args();
    if (\Engine5\Core\Engine::getEntryPoint() != 'rest') {
        echo '<table><tr>';
        foreach ($datas as $data) {
            echo('<td><pre>' . print_r($data, true) . '</pre></td>');
        }
        echo '</tr></table>';
    } else {
        echo json_encode([
            'arguments' => $datas
        ]);
    }
    header('Engin5-Debug-Status: 500');
    header('Engin5-Debug-Entrypoint: ' . \Engine5\Core\Engine::getEntryPoint());
    http_response_code(409);
    die();
}

function d() {
    $datas = func_get_args();
    echo '<table><tr>';
    foreach ($datas as $data) {
        echo('<td><pre>' . print_r($data, true) . '</pre></td>');
    }
    echo '</tr></table>';
}

function fatal_error_handler($buffer) {
    $error = error_get_last();

    if ($error !== null) {
        $error['handler'] = __METHOD__;
        if ($error['type'] == 8192) {

        } elseif ($error['type'] == 8) {
            return false;
        } elseif ($error['type'] == 1) {
            if (preg_match('/^Class \'(?<path>[a-zA-Z0-9]+\\\\Model\\\\)(?<class>[a-zA-Z0-9]+)\' not found/', $error['message'], $matches)) {
                $error['m'] = $matches;

                /* trzeba utworzyć brakującą klasę */

                /* sprawdzamy czy istnieje klasa base */
                $baseClass = $matches['path'] . 'Base\\' . $matches['class'];
                $filename = ST_ROOTDIR . '/apps/model/' . $matches['class'] . '.php';

                if (class_exists($baseClass)) {
                    $text = "<?php

namespace NewBatu\Model;

/**
 * Opis dla klasy {$matches['class']}
 */
class {$matches['class']} extends Base\\{$matches['class']} {

}
";

                    file_put_contents($filename, $text);
                    return 'utworzono plik ' . $filename . ' z brakującą klasą ' . $matches['class'];
                }

                return var_export($error, true);
            }

            http_response_code(500);
            return var_export($error, true);
        } else {
            http_response_code(500);
            return var_export($error, true);
        }
    }
    return $buffer;
}
