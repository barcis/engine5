<?php

namespace Engine5\Tools;

use Symfony\Component\Yaml\Parser,
    Symfony\Component\Yaml\Dumper;

/**
 * Description of Yaml
 *
 * @author barcis
 */
class Yaml {

    public static function parseFile($filename) {
        if (function_exists('yaml_parse_file')) {
            return yaml_parse_file($filename);
        } else {
            $yaml = new Parser();
            return $yaml->parse(file_get_contents($filename));
        }
    }

    public static function saveFile($filename, $content) {
        if (function_exists('yaml_emit')) {
            $data = yaml_emit($content);
        } else {
            $yaml = new Dumper();
            $data = $yaml->dump($content, 4);
            file_put_contents($filename, $data);
        }
    }

}
